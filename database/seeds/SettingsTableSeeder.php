<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('settings')->insert([
            Self::setRaw('use_policy','سياسة الاستخدام هنا'),
            Self::setRaw('about_app','عن التطبيق هنا هنا'),
            Self::setRaw('fb_link', 'https://facebook.com'),
            Self::setRaw('tw_link', 'https://twitter.com'),
            Self::setRaw('site_mail', 'info@goapp.com'),
            Self::setRaw('instagram_link', 'https://instagram.com'),
            Self::setRaw('mobile'),
            Self::setRaw('phone'),
            Self::setRaw('notification_key','AAAA4GKFlxs:APA91bG9Skt4fbkmwKYEU38ZAlLiZB0BB407MOATIHjEtpCf0NU_xfTBJhSbJV2hp824S6FPsB0r1bCvI_xlGHdZY2Q4MCXJzKsaAKWO4wYFSwvHisrh7kENSm7T7_Ub7mVrBg1GuHY9'),
            Self::setRaw('sender_id', '963725596443'),
        ]);
    }

    private static function setRaw($key, $value = null)
    {
        return ['key' => $key, 'value' => $value, 'created_at' => now(), 'updated_at' => now()];
    }
}
