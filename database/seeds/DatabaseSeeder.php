<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(SettingsTableSeeder::class);
                App\Models\Admin::create([
            'name'              => 'Manager',
            'email'             => 'ahmed@email.com',
            'email_verified_at' => now(),
            'password'          => '123456789',
            'mobile'            => "1002700084",
        ]);
    }
}
