<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
    Route::group(['namespace' => 'User', 'prefix' => 'user'], function () {
        Route::any('register', 'LoginController@register');
        Route::any('login', 'LoginController@login');
        Route::any('forgetPassword', 'LoginController@forgetPassword');

        Route::any('categories', 'CategoriesController@index');
        Route::any('subcategories', 'CategoriesController@subcategories');
        Route::any('childcategories', 'CategoriesController@childCategories');
        Route::any('getProvidersByCategoryId', 'CategoriesController@getProvidersByCategoryId');
        Route::any('getProviderById', 'CategoriesController@getProviderById');
        Route::any('getAllOffers', 'CategoriesController@getAllOffers');
    });

    Route::group(['namespace' => 'Provider', 'prefix' => 'provider'], function () {
        Route::any('register', 'LoginController@register');
        Route::any('login', 'LoginController@login');
        Route::any('forgetPassword', 'LoginController@forgetPassword');
        Route::any('subcategories', 'CategoryController@subCategories');
    });

    Route::group(['middleware' => ['apiAuthUser'], 'prefix' => 'user', 'namespace' => 'User'], function () {
        Route::any('maincategories', 'CategoriesController@mainCategories');
        Route::any('subcategries', 'CategoriesController@subCategories');
        Route::any('providers/{order_id}', 'CategoriesController@allProviders');

        Route::any('orders', 'OrderController@AllOrders');
        Route::any('neworder', 'OrderController@newOrder');
        Route::any('order/{order_id}', 'OrderController@getOrder');
        Route::any('setUserPreview', 'OrderController@setUserPreview');
        Route::any('notifications', 'NotificationController@getUserNotifications');

        Route::any('aboutapp', 'SettingController@about');
        Route::any('contactus', 'SettingController@contactus');
        Route::any('privacy', 'SettingController@privacy');
    });

    Route::any('contact', 'User\SettingController@contact');

    Route::group(['middleware' => ['apiAuthProvider'], 'prefix' => 'provider', 'namespace' => 'Provider'], function () {
        Route::any('notifications', 'NotificationController@getProviderNotifications');
        Route::any('orders', 'OrderController@orders');
        Route::any('order/{order_id}', 'OrderController@getOrder');
        Route::any('orderAcceptOrRefuse', 'OrderController@orderAcceptOrRefuse');
        Route::any('getProviderHome', 'OrderController@getProviderHome');
        Route::any('notifications', 'OrderController@notifications');
    });
});



