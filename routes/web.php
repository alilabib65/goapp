<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'dashboard'], function () {

    Route::get('/', 'AuthController@login')->name('login');
    Route::post('/auth', 'AuthController@auth')->name('auth');

    Route::group(['prefix' => 'admin_dashboard', 'as' => 'admin_dashboard.', 'middleware' => ['web', 'auth']], function () {
        Route::get('logout', 'AuthController@logout')->name('logout');
        Route::get('/', 'HomeController@index')->name('index');

        Route::resource('user', 'UserController');
        Route::resource('provider', 'ProviderController');
        Route::resource('service', 'ServiceController');
        Route::resource('offers', 'OffersController');
        Route::resource('subservice', 'SubServiceController');
        Route::resource('order', 'OrderController');

        Route::get('sent', 'ContactController@sent')->name('contact.sent');
        Route::resource('contact', 'ContactController');
        Route::get('setting', 'SettingController@index')->name('setting.index');
        Route::post('setting/store', 'SettingController@store')->name('setting.store');
        Route::get('profile', 'SettingController@profile')->name('setting.profile');
        Route::post('profile/store/{admin}', 'SettingController@updateProfile')->name('setting.profile.update');
    });
});
