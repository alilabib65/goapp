    <nav class="navbar top-navbar">
        <div class="container-fluid">

            <div class="navbar-left">
                <div class="navbar-btn">
                    <a href= "{{ route('admin_dashboard.index')  }}"   ><img src="{{asset('public/dashboard')}}/images/icon.svg" alt="Go Logo" class="img-fluid logo"></a>
                    <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
                </div>
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="icon-envelope"></i>
                            <span class="notification-dot bg-green"> {{ \App\Models\Contact::where('type','recived')->where('readed','0')->count() }} </span>
                        </a>
                        <ul class="dropdown-menu right_chat email vivify fadeIn">
                            <li class="header green">لديك  {{ \App\Models\Contact::where('type','recived')->where('readed','0')->count() }}  رسائل جديدة</li>
                           @forelse (\App\Models\Contact::where('type','recived')->where('readed','0')->get() as $item)
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <div class="avtar-pic w35 bg-red"><span> {{ $item->user->name }} </span></div>
                                            <div class="media-body">
                                                <span class="name"> {{ $item->subject }} </span>
                                                <span class="message">{{ $item->content }}</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                           @empty
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            لايوجد رسائل
                                        </div>
                                    </a>
                                </li>
                           @endforelse

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="icon-bell"></i>
                            <span class="notification-dot bg-azura">0</span>
                        </a>
                        <ul class="dropdown-menu feeds_widget vivify fadeIn">
                            <li class="header blue">لديك  0 إشعارات جديدة</li>
                            {{--  <li>
                                <a href="javascript:void(0);">
                                    <div class="feeds-left bg-red"><i class="fa fa-check"></i></div>
                                    <div class="feeds-body">
                                        <h4 class="title text-danger">Issue Fixed <small class="float-right text-muted">9:10 AM</small></h4>
                                        <small>WE have fix all Design bug with Responsive</small>
                                    </div>
                                </a>
                            </li>  --}}

                        </ul>
                    </li>
                    {{--  <li class="dropdown language-menu">
                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="fa fa-language"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item pt-2 pb-2" href="#"><img src="{{asset('dashboard')}}/images/flag/us.svg " class="w20 mr-2 rounded-circle"> US English</a>
                            <a class="dropdown-item pt-2 pb-2" href="#"><img src="{{asset('dashboard')}}/images/flag/gb.svg " class="w20 mr-2 rounded-circle"> UK English</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item pt-2 pb-2" href="#"><img src="{{asset('dashboard')}}/images/flag/russia.svg " class="w20 mr-2 rounded-circle"> Russian</a>
                            <a class="dropdown-item pt-2 pb-2" href="#"><img src="{{asset('dashboard')}}/images/flag/arabia.svg " class="w20 mr-2 rounded-circle"> Arabic</a>
                            <a class="dropdown-item pt-2 pb-2" href="#"><img src="{{asset('dashboard')}}/images/flag/france.svg " class="w20 mr-2 rounded-circle"> French</a>
                        </div>
                    </li>  --}}
                    {{--  <li><a href="javascript:void(0);" class="megamenu_toggle icon-menu" title="Mega Menu">Mega</a></li>
                    <li class="p_social"><a href="page-social.html" class="social icon-menu" title="News">Social</a></li>
                    <li class="p_news"><a href="page-news.html" class="news icon-menu" title="News">News</a></li>
                    <li class="p_blog"><a href="page-blog.html" class="blog icon-menu" title="Blog">Blog</a></li>  --}}
                </ul>
            </div>

            <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                        {{--  <li><a href="javascript:void(0);" class="search_toggle icon-menu" title="Search Result"><i class="icon-magnifier"></i></a></li>  --}}
                        {{--  <li><a href="javascript:void(0);" class="right_toggle icon-menu" title="Right Menu"><i class="icon-bubbles"></i><span class="notification-dot bg-pink">2</span></a></li>  --}}
                        <li><a href="{{ route('admin_dashboard.logout') }}" class="icon-menu"><i class="icon-power"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="progress-container"><div class="progress-bar" id="myBar"></div></div>
    </nav>
