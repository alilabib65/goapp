    <div id="left-sidebar" class="sidebar">
        <div class="navbar-brand">
            <a href="{{route('admin_dashboard.index')}}">  <img src="{{asset('public/dashboard')}}/images/icon.svg" alt="Oculux Logo" class="img-fluid logo"><span> GO APP </span></a>
            <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="lnr lnr-menu icon-close"></i></button>
        </div>
        <div class="sidebar-scroll">
            <div class="user-account">
                <div class="user_div">
                    <img src="{{asset('public/dashboard')}}/images/user.png" class="user-photo" alt="User Profile Picture">
                </div>
                <div class="dropdown">
                    <span>مرحبا,</span>
                    <a href="{{ route('admin_dashboard.setting.profile') }}" class="dropdown-toggle user-name" data-toggle="dropdown"><strong> إسم المستخدم </strong></a>
                    <ul class="dropdown-menu dropdown-menu-right account vivify flipInY">
                        <li><a href="{{ route('admin_dashboard.setting.profile') }}"><i class="icon-user"></i> حسابي </a></li>
                        <li><a href="{{ route('admin_dashboard.contact.index') }}"><i class="icon-envelope-open"></i>الرسائل</a></li>
                        <li><a href="{{ route('admin_dashboard.setting.index') }}"><i class="icon-settings"></i>الإعدادات</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ route('admin_dashboard.logout') }}"><i class="icon-power"></i>تسجيل الخروج</a></li>
                    </ul>
                </div>
            </div>
            <nav id="left-sidebar-nav" class="sidebar-nav">
                <ul id="main-menu" class="metismenu">
                    <li class="header">القوائم</li>
                    <li class="active">
                        <a href="#myPage" ><i class="icon-home"></i><span>الرئيسية</span></a>
                    </li>

                    {{--  <li class="header">App</li>  --}}

                    <li>
                        <a href="#service" class="has-arrow"><i class="icon-book-open"></i><span>الآقسام الرئيسية للخدمات</span></a>
                        <ul>
                            <li><a href="{{ route('admin_dashboard.service.index') }}"> عرض الكل</a></li>
                            <li><a href="{{ route('admin_dashboard.service.create') }}">إضافة</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="#subservices" class="has-arrow"><i class="icon-bubbles"></i><span>الآقسام الفرعيه للخدمات</span></a>
                        <ul>
                            <li><a href="{{route('admin_dashboard.subservice.index')}}">عرض الكل </a></li>
                            <li><a href="{{route('admin_dashboard.subservice.create')}}">إضافة</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="#providers" class="has-arrow"><i class="icon-tag"></i><span>مقدمين الخدمات</span></a>
                        <ul>
                            <li><a href="{{route('admin_dashboard.provider.index')}}">عرض الكل</a></li>
                            <li><a href="{{route('admin_dashboard.provider.create')}}"> إضافة </a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#uiComponents" class="has-arrow"><i class="icon-diamond"></i><span>المستخدمين</span></a>
                        <ul>
                            <li><a href="{{route('admin_dashboard.user.index')}}"> عرض الكل </a></li>
                            <li><a href="{{route('admin_dashboard.user.create') }}">إضافة</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#forms" class="has-arrow"><i class="icon-pencil"></i><span> الطلبات</span></a>
                        <ul>
                            <li><a href="{{route('admin_dashboard.order.index')}}">عرض الكل</a></li>
                            <li><a href="{{route('admin_dashboard.order.create')}}">إضافة</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#forms" class="has-arrow"><i class="icon-pencil"></i><span>العروض</span></a>
                        <ul>
                            <li><a href="{{route('admin_dashboard.offers.index')}}">عرض الكل</a></li>
                            <li><a href="{{route('admin_dashboard.offers.create')}}">إضافة</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#Tables" class="has-arrow"><i class="icon-layers"></i><span>التواصل </span></a>
                        <ul>
                            <li><a href="{{route('admin_dashboard.contact.index')}}">عرض الكل</a></li>
                            <li><a href="{{route('admin_dashboard.contact.create')}}">إضافة</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#charts" class="has-arrow"><i class="icon-pie-chart"></i><span> الإعدادات </span></a>
                        <ul>
                            <li><a href="{{route('admin_dashboard.setting.index')}}">عرض الكل </a></li>
                            {{--  <li><a href="javascript:void(0);">إضافة</a></li>  --}}
                        </ul>
                    </li>
                    {{--  <li>
                        <a href="#Authentication" class="has-arrow"><i class="icon-lock"></i><span>قسم ٣</span></a>
                        <ul>
                            <li><a href="javascript:void(0);">عرض الكل</a></li>
                            <li><a href="javascript:void(0);">إضافة</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#Pages" class="has-arrow"><i class="icon-docs"></i><span>قسم ٤</span></a>
                        <ul>
                            <li><a href="javascript:void(0);">عرض الكل</a></li>
                            <li><a href="javascript:void(0);">إضافة</a></li>
                        </ul>
                    </li>  --}}
                </ul>
            </nav>
        </div>
    </div>
