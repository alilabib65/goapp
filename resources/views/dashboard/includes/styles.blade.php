    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{asset('public/dashboard')}}/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('public/dashboard')}}/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('public/dashboard')}}/vendor/animate-css/vivify.min.css">
    <link rel="stylesheet" href="{{asset('public/dashboard')}}/vendor/toastr/toastr.min.css">

    <link rel="stylesheet" href="{{asset('public/dashboard')}}/vendor/c3/c3.min.css"/>

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{asset('public/dashboard')}}/css/site.min.css">
    @yield('css')
