<!-- Javascript -->
<script src="{{ asset('public/dashboard') }}/bundles/libscripts.bundle.js"></script>
<script src="{{ asset('public/dashboard') }}/bundles/vendorscripts.bundle.js"></script>
<script src="{{ asset('public/dashboard') }}/vendor/toastr/toastr.js"></script>

<script src="{{ asset('public/dashboard') }}/bundles/c3.bundle.js"></script>

<script src="{{ asset('public/dashboard') }}/bundles/mainscripts.bundle.js"></script>
<script src="{{ asset('public/dashboard') }}/js/index.js"></script>

<script>
    $(function() {
        toastr.options.timeOut = "false";
        toastr.options.closeButton = true;
        toastr.options.positionClass = 'toast-bottom-right';
        {{--  toastr['info']('Hi there, this is notification demo with HTML support. So, you can add HTML elements like <a href="javascript:void(0);">this link</a>');  --}}

        $('.btn-toastr').on('click', function() {
            $context = $(this).data('context');
            $message = $(this).data('message');
            $position = $(this).data('position');

            if ($context === '') {
                $context = 'info';
            }

            if ($position === '') {
                $positionClass = 'toast-bottom-right';
            } else {
                $positionClass = 'toast-' + $position;
            }

            toastr.remove();
            toastr[$context]($message, '', {
                positionClass: $positionClass
            });
        });

    });
</script>
@yield('js')
