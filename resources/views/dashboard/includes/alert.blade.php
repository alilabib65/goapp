@if (session()->has("true"))
    <script>
        $(function () {
            toastr.remove();
            toastr['success']("{{ session()->get("true") }}", '', {
                positionClass: 'toast-top-right'
            });
            toastr.options = {
                "timeOut": "3000",
            }
        });
    </script>
@endif




@if (session()->has("info"))
    <script>
        $(function () {
            toastr.remove();
            toastr['info']("{{ session()->get("info") }}", {
                positionClass: 'toast-top-right'
            });
            toastr.options = {
                "timeOut": "3000",
            }
        });
    </script>
@endif

@if(count($errors) > 0)
    <script>
        @foreach($errors->all() as $error)
        $(function () {
            toastr.remove();
            toastr['error']("{{ $error }}", '', {
                positionClass: 'toast-top-right'
            });
            toastr.options = {
                "timeOut": "3000",
            }
        });
        @endforeach
    </script>

@endif
@if (session()->has("false"))
    <script>
        $(function () {
            toastr.remove();
            toastr['warning']("{{ session()->get("false") }}", '', {
                positionClass: 'toast-top-right'
            });
            toastr.options = {
                "timeOut": "3000",
            }
        });
    </script>

@endif
