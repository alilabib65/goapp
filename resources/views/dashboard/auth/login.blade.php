<!doctype html>
<html lang="en">

<head>
<title>Go APP | تسجيل الدخول</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Go App">
<meta name="author" content="Ali labib">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="{{asset('public/dashboard')}}/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="{{asset('public/dashboard')}}/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="{{asset('public/dashboard')}}/vendor/animate-css/vivify.min.css">
<!-- MAIN CSS -->
    <link rel="stylesheet" href="{{asset('public/dashboard')}}/css/site.min.css">
</head>
<body class="theme-cyan font-montserrat rtl">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
    </div>
</div>

<div class="pattern">
    <span class="red"></span>
    <span class="indigo"></span>
    <span class="blue"></span>
    <span class="green"></span>
    <span class="orange"></span>
</div>

<div class="auth-main particles_js">
    <div class="auth_div vivify popIn">
        <div class="auth_brand">
            <a class="navbar-brand" href="javascript:void(0);"><img src="{{asset('public/dashboard')}}/images/icon.svg" width="30" height="30" class="d-inline-block align-top mr-2" alt="">Go App</a>
        </div>
        <div class="card">
            <div class="body">
                <p class="lead">تسجيل الدخول</p>
                <form class="form-auth-small m-t-20" action="{{route('auth')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="signin-email" class="control-label sr-only">البريد الإلكتروني</label>
                        <input type="email" class="form-control round" id="signin-email" name="email" placeholder="البريد الإلكتروني">
                    </div>
                    <div class="form-group">
                        <label for="signin-password" class="control-label sr-only">الرقم السري</label>
                        <input type="password" class="form-control round" id="signin-password" name="password" placeholder="الرقم السري">
                    </div>
                    <div class="form-group clearfix">
                        <label class="fancy-checkbox element-left">
                            <input type="checkbox" name="remember">
                            <span> تذكرني</span>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-round btn-block">تسجيل الدخول</button>
                    <div class="bottom">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="particles-js"></div>
</div>

<script src="{{ asset('public/dashboard') }}/bundles/libscripts.bundle.js"></script>
<script src="{{ asset('public/dashboard') }}/bundles/vendorscripts.bundle.js"></script>

<script src="{{ asset('public/dashboard') }}/bundles/mainscripts.bundle.js"></script>
        @include('dashboard.includes.scripts')
        @include('dashboard.includes.alert')
</body>
</html>
