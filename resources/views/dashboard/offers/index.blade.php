@extends('dashboard.main.layout')
@section('css')
    <link rel="stylesheet" href="{{asset('public/dashboard') }}/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet"
          href="{{ asset('public/dashboard') }}/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
    <link rel="stylesheet"
          href="{{ asset('public/dashboard') }}/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('public/dashboard') }}/vendor/sweetalert/sweetalert.css"/>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">
                        <li class="breadcrumb-item">
                            <h4 class="page-title">Go App</h4>
                        </li>
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="javascript:void(0);"><i class="fas fa-home"></i> الرئيسية</a>
                        </li>
                        <li class="breadcrumb-item bcrumb-2">
                            <a href="#" onClick="return false;">العروض</a>
                        </li>
                        <li class="breadcrumb-item active">عرض البيانات</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2> العروض <small> الرئيسية </small></h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example dataTable table-custom spacing5">
                                <thead>
                                <tr>
                                    <th> #</th>
                                    <th>الاسم</th>
                                    <th>الوصف</th>
                                    <th>الصوره</th>
                                    <th colspan="2">خيارات</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th> #</th>
                                    <th>الاسم</th>
                                    <th>الوصف</th>
                                    <th>الصوره</th>
                                    <th colspan="2">خيارات</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @forelse ($data as $item)
                                    <tr>
                                        <td> {{ $loop->iteration }} </td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->desc }}</td>
                                        <td>
                                            <img src="{{ $item->image_url }}"
                                                 data-toggle="tooltip" data-placement="top" title="" alt="Avatar"
                                                 class="w35 h35 rounded" data-original-title="Avatar Name"/>
                                        </td>
                                        <td><a class="btn btn-info"
                                               href="{{ route('admin_dashboard.offers.edit',$item->id) }}"> <i
                                                    class="fa fa-edit"></i> تعديل </a></td>
                                        <td>
                                            <form method="post" action="{{route('admin_dashboard.offers.destroy',$item->id) }}"> @method('DELETE') @csrf
                                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> حذف</button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="6"> لايوجد بيانات متوفره</td>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection

@section('js')
    <script src="{{asset('public/dashboard')}}/bundles/datatablescripts.bundle.js"></script>
    <script src="{{asset('public/dashboard') }}/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
    <script src="{{asset('public/dashboard') }}/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('public/dashboard') }}/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
    <script src="{{ asset('public/dashboard') }}/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
    <script src="{{ asset('public/dashboard') }}/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>
    <script src="{{ asset('public/dashboard') }}/vendor/sweetalert/sweetalert.min.js"></script><!-- SweetAlert Plugin Js -->
    <script src="{{ asset('public/dashboard') }}/js/pages/tables/jquery-datatable.js"></script>

@endsection
