@extends('dashboard.main.layout')
@section('css')
<link rel="stylesheet" href="{{asset('public/dashboard')}}/vendor/dropify/css/dropify.min.css">
<link rel="stylesheet" href="{{ asset('public/dashboard') }}/vendor/bootstrap-multiselect/bootstrap-multiselect.css">
<link rel="stylesheet" href="{{ asset('public/dashboard') }}/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
<link rel="stylesheet" href="{{ asset('public/dashboard') }}/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
<link rel="stylesheet" href="{{ asset('public/dashboard') }}/vendor/multi-select/css/multi-select.css">
<style>
        #pac-input{
            width: 390px;
            padding-right: 15px;
        }
</style>
@endsection
@section('content')
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>الطلبات</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="#">الطلبات  </a></li>
                            <li class="breadcrumb-item active" aria-current="page">إضافة</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2> الطلبات</h2>
                        </div>
                        <div class="body">
                            <form id="basic-form" method="post" action="{{route('admin_dashboard.order.store')}}" enctype="multipart/form-data">
                                @csrf
                                @include('dashboard.orders.form')
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
@endsection

@section('js')
<script src="{{ asset('public/dashboard') }}/vendor/dropify/js/dropify.js"></script>
<script src="{{ asset('public/dashboard') }}/bundles/mainscripts.bundle.js"></script>
<script src="{{ asset('public/dashboard') }}/js/pages/forms/dropify.js"></script>
<script src="{{ asset('public/dashboard') }}/vendor/jquery.maskedinput/jquery.maskedinput.min.js"></script>
<script src="{{ asset('public/dashboard') }}/vendor/multi-select/js/jquery.multi-select.js"></script><!-- Multi Select Plugin Js -->
<script src="{{ asset('public/dashboard') }}/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
<script src="{{ asset('public/dashboard') }}/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyDx--BILIM2LjZBxVMjGYVb8YqrY-Vk_Yk&libraries=places&language=ar&callback=initMap" async defer></script>
<script>
        function initMap() {
            var myLatlng = new google.maps.LatLng(31.03896, 31.39949);
            var map , map2;
            var myOptions = {
                zoom: 12,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map  = new google.maps.Map(document.getElementById("map"), myOptions);
            map2 = new google.maps.Map(document.getElementById("map2"), myOptions);
            // marker refers to a global variable
            marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                draggable: true
            });

            marker2 = new google.maps.Marker({
                position: myLatlng,
                map: map2,
                draggable: true
            });

            // Create the search box and link it to the UI element.
            var input  = document.getElementById('pac-input');
            var input2 = document.getElementById('pac-input2');

            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var searchBox2 = new google.maps.places.SearchBox(input2);
            map2.controls[google.maps.ControlPosition.TOP_LEFT].push(input2);
            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

             map2.addListener('bounds_changed', function () {
                searchBox2.setBounds(map2.getBounds());
            });

            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();
                if (places.length == 0) {
                    return;
                }
                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    // marker refers to a global variable
                    marker.setPosition(place.geometry.location);
                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }

                    document.getElementById("lat").value = place.geometry.location.lat();
                    document.getElementById("lng").value = place.geometry.location.lng();
                });
                map.fitBounds(bounds);
            });

            searchBox2.addListener('places_changed', function () {
                var places2 = searchBox2.getPlaces();
                if (places2.length == 0) {
                    return;
                }
                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places2.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    // marker refers to a global variable
                    marker.setPosition(place.geometry.location);
                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }

                    document.getElementById("lat2").value = place.geometry.location.lat();
                    document.getElementById("lng2").value = place.geometry.location.lng();
                });
                map2.fitBounds(bounds);
            });

            google.maps.event.addListener(marker, "dragend", function (event) {
                // get lat/lon of click
                var clickLat = event.latLng.lat();
                var clickLon = event.latLng.lng();
                // show in input box
                document.getElementById("lat").value = clickLat.toFixed(5);
                document.getElementById("lng").value = clickLon.toFixed(5);
            });

            google.maps.event.addListener(marker2, "dragend", function (event) {
                // get lat/lon of click
                var clickLat = event.latLng.lat();
                var clickLon = event.latLng.lng();
                // show in input box
                document.getElementById("lat2").value = clickLat.toFixed(5);
                document.getElementById("lng2").value = clickLon.toFixed(5);
            });
        }
    </script>
@endsection

