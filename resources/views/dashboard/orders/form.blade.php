    <div class="form-group">
        <label for="parent">مقدم الخدمة</label>
        <select id="single-selection" name="provider_id" class="multiselect multiselect-custom form-control">
            <option value=""></option>
            @forelse ($providers as $provider)
                <option value="{{ $provider->id }}" @isset($order)
                    @if ($order->provider_id == $provider->id)
                        selected
                    @endif
                @endisset>{{ $provider->name }}</option>
            @empty

            @endforelse
        </select>
    </div>

    <div class="form-group">
        <label for="parent"> إسم المستخدم </label>
        <select id="single-selection" name="user_id" class="multiselect multiselect-custom form-control">
            <option value=""></option>
            @forelse ($users as $user)
                <option value="{{ $user->id }}" @isset($order)
                    @if ($order->user_id == $user->id)
                        selected
                    @endif
                @endisset>{{ $user->name }}</option>
            @empty

            @endforelse
        </select>
    </div>

    <div class="form-group">
        <label>مكان الطلب</label>
        <input type="text" class="form-control" placeholder="lat" @isset($order)
        value="{{$order->order_lat}}"
        @else  value="31.03896" @endisset name="order_lat" readonly="readonly" id="lat">
                <input type="text" class="form-control" placeholder="lng" @isset($order)
        value="{{$order->order_lng}}"
        @else  value="31.39949" @endisset name="order_lng" readonly="readonly" id="lng">
    </div>

    <div class="form-group row">
            <input type="text" id="pac-input" class="form-control bg-white" placeholder="ابحث عن المكان هنا" />
            <div class="col-lg-12" style="height:400px;" id="map">
            </div>
    </div>


    <div class="form-group">
        <label>مكان الإستلام</label>
        <input type="text" class="form-control" placeholder="lat" @isset($order)
        value="{{$order->receive_lat}}"
        @else  value="31.03896" @endisset name="receive_lat" readonly="readonly" id="lat2">
                <input type="text" class="form-control" placeholder="lng" @isset($order)
        value="{{$order->receive_lng}}"
        @else  value="31.39949" @endisset name="receive_lng" readonly="readonly" id="lng2">
    </div>

    <div class="form-group row">
            <input type="text" id="pac-input2" class="form-control bg-white" placeholder="ابحث عن المكان هنا" />
            <div class="col-lg-12" style="height:400px;" id="map2">
            </div>
    </div>


    <div class="form-group">
        <label>التفاصيل</label>
        <textarea class="form-control" rows="5" cols="30" name="details" placeholder="التفاصيل">@isset($order)
        {{$order->details}}
        @endisset</textarea>
    </div>


    <br>
    <button type="submit" class="btn btn-primary">حفظ</button>


