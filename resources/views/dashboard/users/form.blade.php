    <div class="form-group">
        <label>الإسم</label>
        <input type="text" class="form-control" placeholder="الإسم" @isset($user)
        value="{{$user->name}}"
        @endisset name="name" >
    </div>

       <div class="form-group">
        <label>البريد الإلكتروني</label>
        <input type="email" class="form-control" placeholder="البريد الإلكتروني" @isset($user)
        value="{{$user->email}}"
        @endisset name="email" >
    </div>

    <div class="form-group">
        <label>الهاتف</label>
        <input type="text" class="form-control" placeholder="الهاتف" @isset($user)
        value="{{$user->mobile}}"
        @endisset name="mobile" >
    </div>

    <div class="form-group">
        <label>المدينة</label>
        <input type="text" class="form-control" placeholder="المدينة" @isset($user)
        value="{{$user->city}}"
        @endisset name="city">
    </div>

    <div class="form-group">
        <label>رقم الهاتف</label>
        <input type="text" class="form-control" placeholder="رقم الهاتف" @isset($user)
        value="{{$user->phone}}"
        @endisset name="phone">
    </div>

    <div class="form-group">
        <label>الرقم السري</label>
        <input type="password" class="form-control" placeholder="الرقم السري" name="password">
    </div>



    <br>
    <button type="submit" class="btn btn-primary">حفظ</button>
