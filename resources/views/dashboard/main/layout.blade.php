<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="Go App Service App in mansoura city">
    <meta name="keywords" content="Go App , Go , go , service, traffic">
    <meta name="author" content="Ali Labib">
    <title> GO APP || @yield('title','Home') </title>
    @include('dashboard.includes.styles')
</head>
<body class="theme-cyan font-montserrat rtl">
<!-- Page Loader -->
@include('dashboard.includes.loader')
<!-- #END# Page Loader -->

<!-- Theme Setting -->
@include('dashboard.includes.setting')
<!-- Theme Setting -->

<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- Overlay For Sidebars -->

<div id="wrapper">

    <!-- Top Bar -->
@include('dashboard.includes.navbar')
<!-- #Top Bar -->
    <!-- Mega Menu -->
@include('dashboard.includes.mega')
<!-- Mega Menu -->

    <!-- rightbar -->
@include('dashboard.includes.bar')
<!-- rightbar -->

    <!-- Side Bar -->
@include('dashboard.includes.sidebar')
<!-- Side Bar -->

    <!-- content -->
    <div id="main-content">
        @yield('content')
    </div>
    <!-- content -->
</div>
@include('dashboard.includes.scripts')
@include('dashboard.includes.alert')
</body>
