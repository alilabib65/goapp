    <div class="form-group">
        <label>الإسم</label>
        <input type="text" class="form-control" placeholder="الإسم" @isset($provider)
        value="{{$provider->name}}"
        @endisset name="name" >
    </div>

       <div class="form-group">
        <label>البريد الإلكتروني</label>
        <input type="email" class="form-control" placeholder="البريد الإلكتروني" @isset($provider)
        value="{{$provider->email}}"
        @endisset name="email" >
    </div>

    <div class="form-group">
        <label>الهاتف</label>
        <input type="text" class="form-control" placeholder="الهاتف" @isset($provider)
        value="{{$provider->mobile}}"
        @endisset name="mobile" >
    </div>

    <div class="form-group">
        <label>المدينة</label>
        <input type="text" class="form-control" placeholder="المدينة" @isset($provider)
        value="{{$provider->city}}"
        @endisset name="city">
    </div>

    <div class="form-group">
        <label>رقم الهوية</label>
        <input type="text" class="form-control" placeholder="رقم الهوية" @isset($provider)
        value="{{$provider->identity}}"
        @endisset name="identity">
    </div>

    <div class="form-group">
        <label>الرقم السري</label>
        <input type="password" class="form-control" placeholder="الرقم السري" name="password">
    </div>

    <div class="form-group">
        <label for="parent">القسم الرئيسي</label>
        <select id="single-selection" name="service_id" class="multiselect multiselect-custom form-control">
            <option value=""></option>
            @forelse ($categories as $category)
                <option value="{{ $category->id }}" @isset($provider)
                    @if ($provider->service_id == $category->id)
                        selected
                    @endif
                @endisset>{{ $category->name }}</option>
            @empty

            @endforelse
        </select>
    </div>



    <div class="form-group">
        <label> صورة الهوية </label>
        <input type="file" class="dropify" name="identity_img">
    </div>

    <div class="form-group">
        <label for="parent">الحاله</label>
        <select id="single-selection" name="status" class="multiselect multiselect-custom form-control">
            <option value=""></option>
            <option value="0" @isset($provider)
                @if ($provider->status == 0)
                  selected
                @endif
            @endisset>غير مفعل</option>
            <option value="1" @isset($provider)
                @if ($provider->status == 1)
                  selected
                @endif
            @endisset>مفعل</option>
        </select>
    </div>

    <br>
    <button type="submit" class="btn btn-primary">حفظ</button>
