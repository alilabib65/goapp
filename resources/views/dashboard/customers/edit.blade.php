@extends('dashboard.main.layout')
@section('css')
<link rel="stylesheet" href="{{asset('public/dashboard')}}/vendor/dropify/css/dropify.min.css">
<link rel="stylesheet" href="{{asset('public/dashboard')}}/vendor/dropify/css/dropify.min.css">
<link rel="stylesheet" href="{{ asset('public/dashboard') }}/vendor/bootstrap-multiselect/bootstrap-multiselect.css">
<link rel="stylesheet" href="{{ asset('public/dashboard') }}/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
<link rel="stylesheet" href="{{ asset('public/dashboard') }}/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
<link rel="stylesheet" href="{{ asset('public/dashboard') }}/vendor/multi-select/css/multi-select.css">

@endsection
@section('content')
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>مقدمين  للخدمات</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="#"> الخدمات مقدمين</a></li>
                            <li class="breadcrumb-item active" aria-current="page">تعديل</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2> الخدمات</h2>
                        </div>
                        <div class="body">
                            <form id="basic-form" method="post" action="{{route('admin_dashboard.provider.update',$provider->id)}}" enctype="multipart/form-data">
                                @csrf
                                @method('PATCH')
                                @include('dashboard.customers.form')
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
@endsection

@section('js')
<script src="{{ asset('public/dashboard') }}/vendor/dropify/js/dropify.js"></script>
<script src="{{ asset('public/dashboard') }}/bundles/mainscripts.bundle.js"></script>
<script src="{{ asset('public/dashboard') }}/js/pages/forms/dropify.js"></script>
<script src="{{ asset('public/dashboard') }}/vendor/jquery.maskedinput/jquery.maskedinput.min.js"></script>
<script src="{{ asset('public/dashboard') }}/vendor/multi-select/js/jquery.multi-select.js"></script><!-- Multi Select Plugin Js -->
<script src="{{ asset('public/dashboard') }}/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
<script src="{{ asset('public/dashboard') }}/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
@endsection
