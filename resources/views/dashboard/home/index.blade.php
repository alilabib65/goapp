@extends('dashboard.main.layout')

@section('content')
  <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>لوحة التحكم</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">لوحة التحكم</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> الرئيسية </li>
                            </ol>
                        </nav>
                    </div>
                    {{--  <div class="col-md-6 col-sm-12 text-right hidden-xs">
                        <a href="javascript:void(0);" class="btn btn-sm btn-primary" title="">Create Campaign</a>
                        <a href="https://themeforest.net/item/oculux-bootstrap-4x-admin-dashboard-clean-modern-ui-kit/23091507" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a>
                    </div>  --}}
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-4 col-md-6">
                    <div class="card">
                        <div class="body">
                            <div class="d-flex align-items-center">
                                <div class="icon-in-bg bg-indigo text-white rounded-circle"><i class="fa fa-briefcase"></i></div>
                                <div class="ml-4">
                                    <span> عدد آقسام الخدمات الرئيسية </span>
                                    <h4 class="mb-0 font-weight-medium"> {{ \App\Models\Service::where('parent_id',null)->count() }} </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="card">
                        <div class="body">
                            <div class="d-flex align-items-center">
                                <div class="icon-in-bg bg-azura text-white rounded-circle"><i class="fa fa-credit-card"></i></div>
                                <div class="ml-4">
                                    <span> عدد مقدمين الخدمات  </span>
                                    <h4 class="mb-0 font-weight-medium"> {{ \App\Models\Provider::count() }} </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="card">
                        <div class="body">
                            <div class="d-flex align-items-center">
                                <div class="icon-in-bg bg-orange text-white rounded-circle"><i class="fa fa-users"></i></div>
                                <div class="ml-4">
                                    <span> عدد المستخدمين </span>
                                    <h4 class="mb-0 font-weight-medium"> {{ \App\Models\User::count() }} </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row clearfix">
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>المستخدمين</h2>
                        </div>
                        <div class="body">
                            <div class="row text-center">
                                <div class="col-6 border-right pb-4 pt-4">
                                    <label class="mb-0"> المستخدمين الجدد </label>
                                    <h4 class="font-30 font-weight-bold text-col-blue">0</h4>
                                </div>
                                <div class="col-6 pb-4 pt-4">
                                    <label class="mb-0">  المستخدمين آصحاب الطلبات </label>
                                    <h4 class="font-30 font-weight-bold text-col-blue">0</h4>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="form-group">
                                <label class="d-block">المستخدمين الجدد <span class="float-right">77%</span></label>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="d-block">المستخدمين آصحاب الطلبات <span class="float-right">50%</span></label>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="d-block">مقدمي الخدمات <span class="float-right">23%</span></label>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100" style="width: 23%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="card user_statistics">
                        <div class="header">
                            <h2>تقارير عن الطلبات</h2>
                        </div>
                        <div class="body">
                            <div id="chart-bar" style="height: 302px"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing5">
                            <thead>
                                <tr>
                                    <th style="width: 20px;">#</th>
                                    <th>آخر الطلبات</th>
                                    <th style="width: 50px;">السعر</th>
                                    <th style="width: 50px;">حالة الطلب</th>
                                    <th style="width: 110px;">تحكم</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <span>01</span>
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <div class="avtar-pic w35 bg-red" data-toggle="tooltip" data-placement="top" title="Avatar Name"><span>SS</span></div>
                                            <div class="ml-3">
                                                <a href="#" title="">South Shyanne</a>
                                                <p class="mb-0">south.shyanne@example.com</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td>1200</td>
                                    <td><span class="badge badge-success ml-0 mr-0">Done</span></td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-default" title="إرسال رساله للعميل" data-toggle="tooltip" data-placement="top"><i class="icon-envelope"></i></button>
                                        <button type="button" class="btn btn-sm btn-default " title="طباعة الطلب" data-toggle="tooltip" data-placement="top"><i class="icon-printer"></i></button>
                                        <button type="button" class="btn btn-sm btn-default" title="حذف" data-toggle="tooltip" data-placement="top"><i class="icon-trash"></i></button>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <a class="card" href="javascript:void(0)">
                        <div class="body text-center">
                            <img class="img-thumbnail rounded-circle" src="{{asset('public/dashboard')}}/images/sm/avatar1.jpg" alt="">
                            <h6 class="mt-3">Michelle Green</h6>
                            <div class="text-center text-muted"> العميل الآكثر طلبات </div>
                        </div>
                        <div class="card-footer text-center">
                            <div class="row clearfix">
                                <div class="col-6">
                                    <i class="fa fa-tag font-22"></i>
                                    <div><span class="text-muted">9 طلب</span></div>
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-dollar font-22"></i>
                                    <div><span class="text-muted"> 3.100</span></div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="card">
                        <div class="body">
                            <div class="card-text">
                                <div class="mt-0">
                                    <small class="float-right text-muted">20 </small>
                                    <span>الطلبات قيد الإنتظار</span>
                                    <div class="progress progress-xxs">
                                        <div style="width: 60%;" class="progress-bar"></div>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <small class="float-right text-muted">40 </small>
                                    <span> الطلبات الموافق عليها</span>
                                    <div class="progress progress-xxs">
                                        <div style="width: 50%;" class="progress-bar"></div>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <small class="float-right text-muted">73 </small>
                                    <span>  الطلبات المرفوضة</span>
                                    <div class="progress progress-xxs">
                                        <div style="width: 40%;" class="progress-bar"></div>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <small class="float-right text-muted">40</small>
                                    <span> طلبات تم تنفيذها </span>
                                    <div class="progress progress-xxs mb-0">
                                        <div style="width: 80%;" class="progress-bar bg-danger"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
@endsection

