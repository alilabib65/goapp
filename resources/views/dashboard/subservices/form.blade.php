    <div class="form-group">
        <label>الإسم</label>
        <input type="text" class="form-control" placeholder="الإسم" @isset($subservice)
        value="{{$subservice->name}}"
        @endisset name="name" >
    </div>

    <div class="form-group">
        <label>الوصف</label>
        <textarea class="form-control" rows="5" cols="30" name="desc" placeholder="الوصف">@isset($subservice)
        {{$subservice->desc}}
        @endisset</textarea>
    </div>

    <div class="form-group">
        <label> الصوره </label>
        <input type="file" class="dropify" name="image" >
    </div>
    {{--  <div class="form-group">
        <label>Email Input</label>
        <input type="email" class="form-control" required>
    </div>  --}}

    {{--  <div class="form-group">
        <div class="row clearfix">
            <div class="col-lg-6 col-md-12">
                <label>Checkbox</label>
                <br/>
                <label class="fancy-checkbox">
                    <input type="checkbox" name="checkbox" required data-parsley-errors-container="#error-checkbox">
                    <span>Option 1</span>
                </label>
                <label class="fancy-checkbox">
                    <input type="checkbox" name="checkbox">
                    <span>Option 2</span>
                </label>
                <label class="fancy-checkbox">
                    <input type="checkbox" name="checkbox">
                    <span>Option 3</span>
                </label>
                <p id="error-checkbox"></p>
            </div>
            <div class="col-lg-6 col-md-12">
                <label>Radio Button</label>
                <br />
                <label class="fancy-radio">
                    <input type="radio" name="gender" value="male" required data-parsley-errors-container="#error-radio">
                    <span><i></i>Male</span>
                </label>
                <label class="fancy-radio">
                    <input type="radio" name="gender" value="female">
                    <span><i></i>Female</span>
                </label>
                <p id="error-radio"></p>
            </div>
        </div>
    </div>  --}}
    <div class="form-group">
        <label for="parent">القسم الرئيسي</label>
        <select id="single-selection" name="parent_id" class="multiselect multiselect-custom form-control">
            <option value=""></option>
            @forelse ($categories as $category)
                <option value="{{ $category->id }}" @isset($subservice)
                    @if ($subservice->parent_id == $category->id)
                        selected
                    @endif
                @endisset>{{ $category->name }}</option>
            @empty

            @endforelse
        </select>
    </div>
    <br>
    <button type="submit" class="btn btn-primary">حفظ</button>
