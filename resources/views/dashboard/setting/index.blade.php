@extends('dashboard.main.layout')
@section('css')
    <link rel="stylesheet" href="{{asset('public/dashboard')}}/vendor/dropify/css/dropify.min.css">
    <link rel="stylesheet"
          href="{{ asset('public/dashboard') }}/vendor/bootstrap-multiselect/bootstrap-multiselect.css">
    <link rel="stylesheet"
          href="{{ asset('public/dashboard') }}/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
    <link rel="stylesheet"
          href="{{ asset('public/dashboard') }}/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
    <link rel="stylesheet" href="{{ asset('public/dashboard') }}/vendor/multi-select/css/multi-select.css">

@endsection
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>الآقسام الرئيسية للخدمات</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="#">آقسام الخدمات الرئيسية</a></li>
                            <li class="breadcrumb-item active" aria-current="page">إضافة</li>
                        </ol>
                    </nav>
                </div>

            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h2> الخدمات</h2>
                    </div>
                    <div class="body">
                        <form id="basic-form" method="post" action="{{route('admin_dashboard.setting.store')}}"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>سياسة الإستخدام</label>
                                <textarea class="form-control" rows="5" cols="30" name="use_policy"
                                          placeholder="سياسة الإستخدام">{{ setting('use_policy') }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>عن التطبيق</label>
                                <textarea class="form-control" rows="5" cols="30" name="about_app"
                                          placeholder="عن التطبيق">{{ setting('about_app') }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>بريد التواصل</label>
                                <input type="text" class="form-control" placeholder="الرابط" value="{{ setting('site_mail') }}" name="site_mail">
                            </div>

                            <div class="form-group">
                                <label>رابط فيس بوك</label>
                                <input type="text" class="form-control" placeholder="الرابط" value="{{ setting('fb_link') }}" name="fb_link">
                            </div>

                            <div class="form-group">
                                <label>رابط انستجرام</label>
                                <input type="text" class="form-control" placeholder="الرابط" value="{{ setting('instagram_link') }}" name="instagram_link">
                            </div>

                            <div class="form-group">
                                <label>رابط تويتر</label>
                                <input type="text" class="form-control" placeholder="الرابط" value="{{ setting('tw_link') }}" name="tw_link">
                            </div>

                            <div class="form-group">
                                <label>الموبيل الدعم</label>
                                <input type="text" class="form-control" placeholder="الموبيل " value="{{ setting('mobile') }}" name="mobile">
                            </div>
                            <div class="form-group">
                                <label>هاتف الدعم</label>
                                <input type="text" class="form-control" placeholder="هاتف " value="{{ setting('phone') }}" name="phone">
                            </div>
                            <div class="form-group">
                                <label>Notification Server Key</label>
                                <textarea class="form-control" rows="3" placeholder="Notification Server Key" name="notification_key">{{ setting('notification_key') }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Sender ID</label>
                                <input type="text" class="form-control" placeholder="Sender ID" value="{{ setting('sender_id') }}" name="sender_id">
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection

@section('js')
    <script src="{{ asset('public/dashboard') }}/vendor/dropify/js/dropify.js"></script>
    <script src="{{ asset('public/dashboard') }}/bundles/mainscripts.bundle.js"></script>
    <script src="{{ asset('public/dashboard') }}/js/pages/forms/dropify.js"></script>
    <script src="{{ asset('public/dashboard') }}/vendor/jquery.maskedinput/jquery.maskedinput.min.js"></script>
    <script
        src="{{ asset('public/dashboard') }}/vendor/multi-select/js/jquery.multi-select.js"></script><!-- Multi Select Plugin Js -->
    <script src="{{ asset('public/dashboard') }}/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
    <script src="{{ asset('public/dashboard') }}/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
@endsection
