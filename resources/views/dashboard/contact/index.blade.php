@extends('dashboard.main.layout')
@section('css')
<link rel="stylesheet" href="{{asset('public/dashboard') }}/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('public/dashboard') }}/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('public/dashboard') }}/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('public/dashboard') }}/vendor/sweetalert/sweetalert.css"/>


@endsection

@section('content')
<div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ul class="breadcrumb breadcrumb-style ">
                    <li class="breadcrumb-item">
                        <h4 class="page-title">Go App</h4>
                    </li>
                    <li class="breadcrumb-item bcrumb-1">
                        <a href="">
                            <i class="fas fa-home"></i> الرئيسية</a>
                    </li>
                    <li class="breadcrumb-item bcrumb-2">
                        <a href="#" onClick="return false;">التواصل</a>
                    </li>
                    <li class="breadcrumb-item active">عرض البيانات</li>
                </ul>
            </div>
        </div>
    </div>

                <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="mail-inbox">
                            <div class="mobile-left">
                            </div>
                            <div class="body mail-left">
                                <div class="mail-compose m-b-20">
                                    <a href="{{ route('admin_dashboard.contact.create') }}" class="btn btn-danger btn-block btn-round">إنشاء رسالة</a>
                                </div>
                                <div class="mail-side">
                                    <ul class="nav">
                                        <li class="active"><a href="{{route('admin_dashboard.contact.index')}}"><i class="icon-drawer"></i>الوارده<span class="badge badge-primary float-right">{{ \App\Models\Contact::where('type','recived')->count() }}</span></a></li>
                                        <li><a href="{{route('admin_dashboard.contact.sent')}}"><i class="icon-cursor"></i>المرسله<span class="badge badge-warning float-right">{{ \App\Models\Contact::where('type','sent')->count() }}</span></a></li>

                                    </ul>

                                </div>
                            </div>
                            <div class="body mail-right check-all-parent">

                                <div class="mail-action clearfix m-l-15">
                                    <div class="pull-left">
                                    </div>
                                </div>
                                <div class="mail-list">
                                    <ul class="list-unstyled">
                                        @forelse ($data as $item)
                                        <li class="clearfix">
                                            <div class="md-left">
                                            <td><form method="post" action="{{route('admin_dashboard.contact.destroy',$item->id) }}"> @method('DELETE') @csrf <button type="submit" class="btn btn-danger" style="width: 90%;margin-top: -13px;"> <i class="fa fa-trash-o"></i> حذف </button>  </form></td>
                                            </div>
                                            <div class="md-right">
                                                <img class="rounded" src="{{ asset('public/dashboard') }}/images/xs/avatar2.jpg" alt="">
                                                <p class="sub"><a href="{{route('admin_dashboard.contact.show',$item->id)}}" class="mail-detail-expand">@if($item->user_id != null) {{ $item->user->name }} @elseif($item->provider_id != null) {{ $item->provider->name }} @endif</a></p>
                                                <p class="dep"><span class="m-r-10">[الدعم]</span> {{ $item->content }} </p>
                                                <span class="time"><i class="fa fa-paperclip"></i> {{ $item->created_at->diffForHumans() }}</span>
                                            </div>
                                        </li>
                                        @empty
                                          <li class="clearfix">
                                            <div class="md-right">
                                                <p class="dep"> لايوجد بيانات متاحه </p>
                                            </div>
                                        </li>
                                        @endforelse

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

</div>
@endsection

@section('js')
<script src="{{asset('public/dashboard')}}/bundles/datatablescripts.bundle.js"></script>
<script src="{{asset('public/dashboard') }}/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="{{asset('public/dashboard') }}/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="{{ asset('public/dashboard') }}/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="{{ asset('public/dashboard') }}/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="{{ asset('public/dashboard') }}/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="{{ asset('public/dashboard') }}/vendor/sweetalert/sweetalert.min.js"></script><!-- SweetAlert Plugin Js -->
<script src="{{ asset('public/dashboard') }}/js/pages/tables/jquery-datatable.js"></script>

@endsection
