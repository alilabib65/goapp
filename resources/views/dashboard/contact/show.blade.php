@extends('dashboard.main.layout')
@section('css')
<link rel="stylesheet" href="{{asset('dashboard') }}/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('dashboard') }}/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('dashboard') }}/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('dashboard') }}/vendor/sweetalert/sweetalert.css"/>


@endsection

@section('content')
<div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ul class="breadcrumb breadcrumb-style ">
                    <li class="breadcrumb-item">
                        <h4 class="page-title">Go App</h4>
                    </li>
                    <li class="breadcrumb-item bcrumb-1">
                        <a href="">
                            <i class="fas fa-home"></i> الرئيسية</a>
                    </li>
                    <li class="breadcrumb-item bcrumb-2">
                        <a href="#" onClick="return false;">التواصل</a>
                    </li>
                    <li class="breadcrumb-item active">عرض البيانات</li>
                </ul>
            </div>
        </div>
    </div>

                <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="mail-inbox">
                            <div class="mobile-left">
                            </div>
                            <div class="body mail-left">
                                <div class="mail-compose m-b-20">
                                    <a href="{{ route('admin_dashboard.contact.create') }}" class="btn btn-danger btn-block btn-round">Compose</a>
                                </div>
                                <div class="mail-side">
                                    <ul class="nav">
                                        <li class="active"><a href="{{route('admin_dashboard.contact.index')}}"><i class="icon-drawer"></i>الوارده<span class="badge badge-primary float-right">6</span></a></li>
                                        <li><a href="{{route('admin_dashboard.contact.sent')}}"><i class="icon-cursor"></i>المرسله<span class="badge badge-warning float-right">8</span></a></li>

                                    </ul>

                                </div>
                            </div>
                            <div class="body mail-right check-all-parent">
                                <div class="mail-detail-full">
                                    <div class="mail-action clearfix">

                                    </div>
                                    <div class="detail-header">
                                        <div class="media">
                                            <div class="float-left">
                                                <div class="m-r-20"><img src="{{ asset('dashboard') }}/assets/images/sm/avatar1.jpg" class="rounded" alt=""></div>
                                            </div>
                                            <div class="media-body">
                                                <p class="mb-0"><strong class="text-muted m-r-5">From :</strong><a class="text-default" href="javascript:void(0);">info@puffintheme.com</a><span class="text-muted text-sm float-right">12:48, 23.06.2018</span></p>
                                                <p class="mb-0"><strong class="text-muted m-r-5">To :</strong>Me <small class="text-muted float-right"><i class="zmdi zmdi-attachment m-r-5"></i>(2 files, 89.2 KB)</small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mail-cnt">
                                        <p>Hello <strong>Marshall Nichols</strong>,</p><br>
                                        <p>Lorem Ipsum is simply dummy text of only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                                        <ul>
                                            <li>standard dummy text ever since the 1500s, when an unknown printer</li>
                                            <li>Lorem Ipsum has been the industrys standard dummy</li>
                                        </ul>
                                        <p>Thank youm,<br><strong>Michael Bradley</strong></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


</div>
@endsection

@section('js')
<script src="{{asset('dashboard')}}/bundles/datatablescripts.bundle.js"></script>
<script src="{{asset('dashboard') }}/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="{{asset('dashboard') }}/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="{{ asset('dashboard') }}/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="{{ asset('dashboard') }}/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="{{ asset('dashboard') }}/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="{{ asset('dashboard') }}/vendor/sweetalert/sweetalert.min.js"></script><!-- SweetAlert Plugin Js -->
<script src="{{ asset('dashboard') }}/js/pages/tables/jquery-datatable.js"></script>

@endsection
