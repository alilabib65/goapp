    <div class="form-group">
        <label class="fancy-radio"><input name="from" value="user" type="radio" @isset($contact)
            @if ($contact->from == 'user')
                checked
            @endif
        @endisset><span><i></i>مستخدم</span></label>
        <label class="fancy-radio"><input name="from" value="provider" type="radio" @isset($contact)
            @if ($contact->from == 'provider')
                checked
            @endif
        @endisset  ><span><i></i>مقدم خدمة</span></label>
    </div>
    <div class="form-group" id="providers">
        <label for="parent">مقدم الخدمة</label>
        <select id="single-selection" name="provider_id" class="multiselect multiselect-custom form-control">
            <option value=""></option>
            @forelse ($providers as $provider)
                <option value="{{ $provider->id }}" @isset($order)
                    @if ($order->provider_id == $provider->id)
                        selected
                    @endif
                @endisset>{{ $provider->name }}</option>
            @empty

            @endforelse
        </select>
    </div>


    <div class="form-group" id="users">
        <label for="parent"> إسم المستخدم </label>
        <select id="single-selection" name="user_id" class="multiselect multiselect-custom form-control">
            <option value=""></option>
            @forelse ($users as $user)
                <option value="{{ $user->id }}" @isset($order)
                    @if ($order->user_id == $user->id)
                        selected
                    @endif
                @endisset>{{ $user->name }}</option>
            @empty

            @endforelse
        </select>
    </div>

    <div class="form-group">
        <label>عنوان الرساله</label>
        <input type="text" class="form-control" placeholder="عنوان الرساله" @isset($service)
        value="{{$contact->subject}}"
        @endisset name="subject" >
    </div>

    <div class="form-group">
        <label>تفاصيل الرساله</label>
        <textarea class="form-control" rows="5" cols="30" name="content" placeholder="الرساله تفاصيل">@isset($contact)
        {{$contact->desc}}
        @endisset</textarea>
    </div>


    <br>
    <button type="submit" class="btn btn-primary">حفظ</button>
