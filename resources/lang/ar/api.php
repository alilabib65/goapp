<?php

return [
	'provider-registeration-message' => 'تم إرسال بياناتك للادارة للتأكد من صحة البيانات وسيتم الرد عليك عن طريق البريد الالكتروني في اقرب وقت وشكرا لتعاملكم معنا',

	'request-done-successfully' 	=> 'الطلب تم بنجاح',
	'hall-is-not-found' 			=> 'عفوا هذه الصاله غير موجودة',
	'category-is-not-found' 		=> 'عفوا هذه الفئة غير موجودة',
	'provider-is-not-found' 		=> 'عفوا مقدم الخدمة هذا غير موجود',
	'reservation-is-not-found' 		=> 'عفوا هذا الحجز غير موجود',
	'server-internal-error' 		=> 'خطأ في الخادم',
	'item-deleted-successfully' 	=> 'تم حذف المنتج من الحجز بنجاح',
	'item-not-deleted-successfully' => 'حدث خطأ اثناء حذف المنتج من القائمة',

	'reservation-done-successfully' => 'عميلنا العزيز تم الانتهاء من عملية الخجز بنجاح وسيتم التواصل معك هاتفيا او عن طريق البريد الاكتروني',
	'payment-for-package'			=> 'عفوا يجب عليك  الاشتراك في باقة اولا',
	'the-code-is-valid'				=> 'الكود المرسل صحيح',
	'the-code-is-invalid'		    => 'الكود المرسل  غير صحيح',
	'mail-send-successfully'		=> 'تم ارسال البريد الالكتروني بنجاح',
	'password-is-not-matched'  		=> 'عفوا كلمة المرور غير متتابقة',
	'delete-advertisement-alert'    => 'عفوا يجب عليك سداد قيمة عمولة التطبيق اولا قبل حذف الاعلان وهي :value ريال',
	'advertisement-is-not-for-this-user' => 'عفوا هذا الاعلان ليس ملك لهذا المستخدم',
	'can-not-send-message-to-user-self' => 'عفوا لا يمكن ان ترسل رسالة الي نفسك',
	'receive-id-is-not-found'		=> 'عفوا المستلم غير موجود',
	'invalid-transaction-info'		=> 'عفوا عملية الدفع هذه غير موجوده',
	'you-must-renew-your-subscribe' => 'عفوا يجب عليك تجديد إشتراكك اولا قبل الحذف',
	'sorry-you-reach-the-max-num-of-ads' => 'عفوا تم تعدي الحد الاقصي المسموح بيه خلال الباقة الحالية',
];