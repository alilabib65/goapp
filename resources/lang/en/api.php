<?php

return [
	'provider-registeration-message' => 'Your data has been sent to the administration to confirm the accuracy of the data and will be answered by e-mail as soon as possible. Thank you for your cooperation with us',
	
	'request-done-successfully' 	=> 'Request Done Successfully',
	'hall-is-not-found' 			=> 'Sorry, this hall is not found',
	'category-is-not-found' 		=> 'Sorry, this category is not found',
	'provider-is-not-found' 		=> 'Sorry, this provider is not found',
	'reservation-is-not-found' 		=> 'Sorry, this reservation is not found',
	'server-internal-error' 		=> 'Internal Server Error',
	'item-deleted-successfully' 	=> 'Item Removed Successfully',
	'item-not-deleted-successfully' => 'Item not Removed from list',

	'reservation-done-successfully' => 'Dear customer, the booking process has been successfully completed and you will be contacted by telephone or by e-mail',
	'payment-for-package'			=> 'You must subscribe to a package first !',
	'the-code-is-invalid'			=> 'The Code is invalid',
	'the-code-is-valid'  			=> 'The Code is valid',
	'mail-send-successfully'  		=> 'Mail Send Successfully',
	'password-is-not-matched'  		=> 'Password is not Matched',
	'delete-advertisement-alert'	=> 'Sorry, you must pay application tax first before deleting the advertisement and it is :value SR',
	'advertisement-is-not-for-this-user' => 'Advertisement is not for this user',
	'can-not-send-message-to-user-self' => 'Can not send a message to your self',
	'receive-id-is-not-found'		=> 'Receive id is not found',
	'invalid-transaction-info'		=> 'Sorry, this payment proccess is not found',
	'you-must-renew-your-subscribe' => 'Sorry, you must renew your subscribtion first',
	'sorry-you-reach-the-max-num-of-ads' => 'Sorry, the maximum allowance for the current package is exceeded',
];