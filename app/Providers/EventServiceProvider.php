<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\SendResetCodeMailEvent;
use App\Listeners\SendResetCodeMailListener;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        Registered::class => [SendEmailVerificationNotification::class],
        SendResetCodeMailEvent::class => [SendResetCodeMailListener::class],
    ];

    public function boot()
    {
        parent::boot();

        //
    }
}
