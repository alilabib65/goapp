<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendResetCodeMailEvent
{
    use Dispatchable, SerializesModels;

    public $mess;
    public $user;

    public function __construct($user, $mess)
    {
        $this->mess = $mess;

        $this->user = $user;
    }
}
