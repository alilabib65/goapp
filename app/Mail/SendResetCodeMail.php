<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendResetCodeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mess;

    public function __construct($mess)
    {
        $this->mess;
    }

    public function build()
    {
        return $this->view('emails.SendMail')->with('MSG', $this->mess);
    }
}
