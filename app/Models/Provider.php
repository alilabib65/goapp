<?php

namespace App\Models;
use App\Events\SendResetCodeMailEvent;
use App\Http\Resources\ProviderResource;
use Exception;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Model;

class Provider extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'device_token'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    public function token()
    {
        return $this->hasOne('App\Models\ProviderToken');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\ProviderNotification','provider_id','id');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

    public function getIdentityImgUrlAttribute()
    {
        if($this->identity_img != '') return url('public/uploaded/providers/' . $this->identity_img);
        else return url('public/dashboard/images/user.png');
    }

    public function setPasswordAttribute($password)
    {
        if ( !empty($password) ) {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    public static function apiProviderLogin($request)
    {
        $provider = Provider::whereMobile($request->mobile)->first();

        if($provider->status == 0) return apiResponse([], 'success', null, 'عفوا هذا الحساب غير مفعل من الادارة حاليا', 203);

        if (!$token = Auth::guard('provider')->attempt($request->only('mobile','password'))) return apiResponse([],'success',null,trans('auth.failed'), 203);

        if(!auth()->guard('provider')->user()->token)
        {
            ProviderToken::createToken($request, auth()->guard('provider')->user(), $token);
        }

        else auth()->guard('provider')->user()->token->update(['jwt' => $token, 'ip' => $request->ip()]);

        if(!auth()->guard('provider')->user()->fcm)
        {
            FcmProvider::create(['provider_id' => auth()->guard('provider')->user()->id,'fcm_token' => $request->device_token]);
        }

        else auth()->guard('provider')->user()->fcm->update(['fcm_token' => $request->device_token]);

        return apiResponse(new ProviderResource($provider), 'success');
    }

    public static function apiProviderRegister($request)
    {
        DB::beginTransaction();
        try
        {
            $providerData = $request->all();

            if($request->hasFile('identity_img')) $providerData['identity_img'] = uploaded($request->identity_img,'provider');

            else $providerData['identity_img'] = arr()->except($providerData,['identity_img']);

            $createdProvider = Provider::create($providerData);

            // get the user token;
            $jwt = Auth::guard('provider')->fromUser($createdProvider);

            // add the user id and generated token;
            ProviderToken::createToken($request, $createdProvider, $jwt);

            // create or update firebase token;
            FcmProvider::createFcmProvider($createdProvider, $request);

            DB::commit();

            return apiResponse(new ProviderResource($createdProvider), 'success');
        }
        catch(Exception $e)
        {
            DB::rollBack();
            return apiResponse([], 'fails', null, $e->getMessage());
        }
    }

    public static function apiForgetPassword($request)
    {
        try
        {
            $provider = Provider::whereEmail($request->email)->first();

            $code = create_rand_numbers(8);

            $provider->update(['code' => $code]);

            $mess = "كود إعادة تعين كلمة المرور الخاص بك هو : $code";

            event(new SendResetCodeMailEvent($provider, $mess));

            return apiResponse([], 'success', null, 'تم ارسال كود اعادة التعين بنجاح');
        }
        catch (Exception $e)
        {
            return apiResponse([], 'fails', null, $e->getMessage());
        }
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'provider_id', 'id');
    }
}
