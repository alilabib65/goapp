<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $guarded = ['id'];

    protected $date   = ['created_at','updated_at'];

    public static function apiCreateContact($request)
    {
        try
        {
            $contact = Contact::create([
                'subject'     => $request->subject,
                'content'     => $request->content,
                'from'        => $request->from,
                'user_id'     => auth()->guard('api')->user() ? auth()->guard('api')->user()->id : null,
                'provider_id' => auth()->guard('provider')->user() ? auth()->guard('provider')->user()->id : null,
            ]);
            return apiResponse([], 'success');
        }
        catch(Exception $e)
        {
            return apiResponse([],'fails',null,$e->getMessage());
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class, 'provider_id', 'id');
    }
}
