<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
	protected $guarded = ['id'];

    protected $table = "tokens";

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }

    public static function createToken($request, $createdUser, $jwt)
    {
        return Token::create(['user_id'=>$createdUser->id,'jwt'=>$jwt,'ip'=>$request->ip()]);
    }
}
