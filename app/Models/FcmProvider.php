<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FcmProvider extends Model
{
    protected $guarded = ['id'];

    public function provider()
    {
    	return $this->belongsTo('App\Models\Provider');
    }

    public static function createFcmProvider($createdProvider, $request)
    {
        return FcmProvider::create(['provider_id' => $createdProvider->id, 'fcm_token' => $request->device_token]);
    }

    public static function getInSelectForm($with_main = null , $with_null = true, $exceptedIds = [] )
    {
        $providers   = [];

        $with_null == null ? $providers = $providers : $providers = $providers + [''  => ''];
        $with_main == null ? $providers = $providers : $providers = $providers + ['0' => 'Main'];

        $providersDB = FcmProvider::whereNotIn('id',$exceptedIds)

        ->leftJoin('providers', 'fcm_providers.provider_id', '=', 'providers.id')

        ->select(['providers.name', 'fcm_providers.fcm_token'])->get();

        foreach ($providersDB as $provider) { $providers[$provider->fcm_token] = ucwords($provider->name); }

        return $providers;
    }
}
