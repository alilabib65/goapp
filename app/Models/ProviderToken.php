<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderToken extends Model
{
    protected $guarded = ['id'];

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public static function createToken($request, $createdProvider, $jwt)
    {
        return ProviderToken::create(['provider_id'=>$createdProvider->id,'jwt'=>$jwt,'ip'=>$request->ip()]);
    }
}
