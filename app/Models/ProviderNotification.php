<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderNotification extends Model
{
    protected $guarded = ['id'];
}
