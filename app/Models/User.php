<?php

namespace App\Models;

use App\Events\SendResetCodeMailEvent;
use App\Http\Resources\SingleUser;
use Exception;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Facades\JWTAuth;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'mobile' ,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function token()
    {
        return $this->hasOne('App\Models\Token');
    }

    public function setPasswordAttribute($password)
    {
        if ( !empty($password) ) {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\UserNotification','user_id','id');
    }

    public static function apiUserLogin($request)
    {
        $perms = ['phone' => $request->phone, 'password' => $request->password];

        $user = User::wherePhone($request->phone)->first();

        if (!$token = Auth::guard('api')->attempt($perms)) return apiResponse([],'success',null,trans('auth.failed'), 203);

        if(!auth()->guard('api')->user()->token)
        {
            Token::createToken($request, auth()->guard('api')->user(), $token);
        }

        else auth()->guard('api')->user()->token->update(['jwt' => $token, 'ip' => $request->ip()]);

        if(!auth()->guard('api')->user()->fcm)
        {
            FcmUser::create(['user_id' => auth()->guard('api')->user()->id,'fcm_token' => $request->device_token]);
        }

        else auth()->guard('api')->user()->fcm->update(['fcm_token' => $request->device_token]);

        return apiResponse(new SingleUser($user), 'success');
    }

    public static function apiUserRegister($request)
    {
        DB::beginTransaction();
        try
        {
            $createdUser = User::create($request->except(['device_token']));

            // get the user token;
            $jwt = JWTAuth::fromUser($createdUser);

            // add the user id and generated token;
            $token = Token::createToken($request, $createdUser, $jwt);

            // create or update firebase token;
            FcmUser::createFcmUser($createdUser, $request);

            DB::commit();

            return apiResponse(new SingleUser($createdUser), 'success');
        }
        catch(Exception $e)
        {
            DB::rollBack();
            return apiResponse([], 'fails', null, $e->getMessage());
        }
    }


    public static function apiForgetPassword($request)
    {
        try
        {
            $user = User::whereEmail($request->email)->first();

            $code = create_rand_numbers(8);

            $user->update(['code' => $code]);

            $mess = "كود إعادة تعين كلمة المرور الخاص بك هو : $code";

            event(new SendResetCodeMailEvent($user, $mess));

            return apiResponse([], 'success', null, 'تم ارسال كود اعادة التعين بنجاح');
        }
        catch (Exception $e)
        {
            return apiResponse([], 'fails', null, $e->getMessage());
        }
    }
    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id', 'id');
    }
}
