<?php

namespace App\Models;

use App\Http\Resources\OrderResource;
use Exception;
use App\Models\Review;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class, 'provider_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }

    public static function apiMakeNewOrder($request)
    {
        try
        {
            $auth = auth()->guard('api')->user();

            $body = "قام $auth->name بعمل طلب جديد";

            $order = Order::create([
                'user_id'     => $auth->id,
                'provider_id' => $request->provider_id,
                'service_id'  => $request->service_id,
                'status'      => 'wait',
                'order_lat'   => $request->order_lat,
                'order_lng'   => $request->order_lng,
                'receive_lat' => $request->receive_lat,
                'receive_lng' => $request->receive_lng,
                'wait_date'   => now()->format('Y-m-d'),
                'details'     => $request->details,
            ]);

            push_notification('طلب جديد', $body, $auth->fcm_token);

            return apiResponse(new OrderResource($order),'success',null,'تم الطلب بنجاح');
        }
        catch(Exception $e)
        {
            return apiResponse([], 'fails', null, $e->getMessage());
        }
    }

    public static function apiSetUserPreview($request)
    {
        try
        {
            Review::create([
                'user_id' => auth()->guard('api')->user()->id,
                'provider_id' => $request->provider_id,
                'comment' => $request->comment ?? null,
                'rate' => $request->rate
            ]);

            return apiResponse([], 'success', null, 'تم الطلب بنجاح');
        }
        catch (Exception $e)
        {
            return apiResponse([], 'fails', null, $e->getMessage());
        }
    }

    public static function apiOrderAcceptOrRefuse($request)
    {
        $order = Order::find($request->order_id);

        if($request->action == 'accept')
        {
            $order->status = 'receive';
            $order->receive_date = now()->format('Y-m-d');
            $order->save();
            return apiResponse([],'success',null,'تم قبول الطلب بنجاح');
        }
        else
        {
            $order->status = 'refuse';
            $order->wait_date = null;
            $order->save();
            return apiResponse([],'success',null,'تم رفض الطلب');
        }
    }
}
