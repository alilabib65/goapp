<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $guarded = ['id'];

    public function getImageUrlAttribute()
    {
        if($this->img != '') return asset('storage/app/public/images') .'/'. $this->img;
        return url('/public/dashboard/images/user.png');
    }
}
