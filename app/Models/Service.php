<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo(Service::class, 'parent_id', 'id');
    }

    public function subcategories()
    {
        return $this->hasMany(Service::class, 'parent_id', 'id');
    }

    public function providers()
    {
        return $this->hasMany(Provider::class, 'service_id', 'id');
    }

    public function getImageUrlAttribute()
    {
        if($this->img != '') return asset('storage/app/public/images') .'/'. $this->img;
        return url('/dashboard/images/user.png');
    }
}
