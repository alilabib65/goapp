<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $guarded = ['id'];

    public function scopeGetContactUs($query)
    {
        return $query->where('key', 'fb_link')

            ->orWhere('key', 'tw_link')

            ->orWhere('key', 'site_mail')

            ->orWhere('key', 'instagram_link')

            ->orWhere('key', 'mobile')

            ->orWhere('key', 'phone');
    }
}
