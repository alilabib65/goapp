<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FcmUser extends Model
{
    protected $guarded = ['id'];

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }

    public static function createFcmUser($createdUser, $request)
    {
        return FcmUser::create(['user_id' => $createdUser->id, 'fcm_token' => $request->device_token]);
    }

    public static function getInSelectForm($with_main = null , $with_null = true, $exceptedIds = [] )
    {
        $users   = [];

        $with_null == null ? $users = $users : $users = $users + [''  => ''];
        $with_main == null ? $users = $users : $users = $users + ['0' => 'Main'];

        $usersDB = FcmUser::whereNotIn('id',$exceptedIds)

        ->leftJoin('users', 'fcm_users.user_id', '=', 'users.id')

        ->select(['users.name', 'fcm_users.fcm_token'])->get();

        foreach ($usersDB as $user) { $users[$user->fcm_token] = ucwords($user->name); }
        
        return $users;
    }
}
