<?php

namespace App\Http\Middleware;

use Closure;

class apiAuthProvider
{
    public function handle($request, Closure $next)
    {
        if(auth()->guard('provider')->check()) return $next($request);

        return apiResponse([], 'success', null, 'عفوا يجب عليك تسجيل الدخول اولا', 203);
    }
}
