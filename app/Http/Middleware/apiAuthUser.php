<?php

namespace App\Http\Middleware;

use Closure;

class apiAuthUser
{
    public function handle($request, Closure $next)
    {
        if(auth()->guard('api')->check()) return $next($request);

        return apiResponse([], 'success', null, 'عفوا يجب عليك تسجيل الدخول اولا', 203);
    }
}
