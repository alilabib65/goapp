<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\MakeNewOrderRequest;
use App\Http\Requests\Api\setUserPreviewRequest;
use App\Http\Resources\OrderResource;
use App\Http\Resources\UserOrderResource;
use App\Models\Order;

class OrderController extends Controller
{
    public function AllOrders()
    {
        return apiResponse(new UserOrderResource(auth()->guard('api')->user()->orders), 'success');
    }

    public function newOrder(MakeNewOrderRequest $request)
    {
        return Order::apiMakeNewOrder($request);
    }

    public function getOrder($order_id)
    {
        if(!$order = Order::find($order_id)) return response()->json(['data' => [], 'message' => 'عفوا هذا الطلب غير موجود', 'status' => false], 422);

        return apiResponse(new OrderResource($order),'success');
    }

    public function setUserPreview(setUserPreviewRequest $request)
    {
        return Order::apiSetUserPreview($request);
    }
}
