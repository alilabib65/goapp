<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationResource;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function getUserNotifications()
    {
        return apiResponse(NotificationResource::collection(auth()->guard('api')->user()->notifications),'success');
    }
}
