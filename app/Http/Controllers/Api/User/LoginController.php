<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\PARENT_API;
use App\Http\Requests\Api\ForgetPasswordRequest;
use App\Http\Requests\Api\UserRegisterRequest;
use App\Http\Requests\Api\UserLoginRequest;
use App\Models\User;
use Illuminate\Http\Request;

class LoginController extends PARENT_API
{
    public function login(UserLoginRequest $request)
    {
        return User::apiUserLogin($request);
    }

    public function register(UserRegisterRequest $request)
    {
        return User::apiUserRegister($request);
    }

    public function forgetPassword(ForgetPasswordRequest $request)
    {
        return User::apiForgetPassword($request);
    }
}
