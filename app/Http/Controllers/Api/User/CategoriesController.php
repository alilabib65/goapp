<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\childCategoriesRequest;
use App\Http\Requests\Api\GetProviderByIdRequest;
use App\Http\Resources\OfferResource;
use App\Http\Resources\ProviderResource;
use App\Models\Offer;
use App\Models\Provider;
use Illuminate\Http\Request;
use App\Models\Service;
use App\Http\Resources\CategoryResource;

class CategoriesController extends Controller
{
    public function index()
    {
        return apiResponse(CategoryResource::collection(Service::where('parent_id',null)->get()), 'success');
    }

    public function subcategories()
    {
        return apiResponse(CategoryResource::collection(Service::where('parent_id','!=',null)->get()), 'success');
    }

    public function getProvidersByCategoryId(childCategoriesRequest $request)
    {
        return apiResponse(ProviderResource::collection(Service::find($request->category_id)->providers), 'success');
    }

    public function getProviderById(GetProviderByIdRequest $request)
    {
        return apiResponse(new ProviderResource(Provider::find($request->provider_id)), 'success');
    }

    public function childCategories(childCategoriesRequest $request)
    {
        return apiResponse(CategoryResource::collection(Service::where('parent_id','=',$request->category_id)->get()), 'success');
    }

    public function getAllOffers()
    {
        return apiResponse(OfferResource::collection(Offer::get()), 'success');
    }
}
