<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\childCategoriesRequest;
use App\Http\Requests\Api\contactUsRequest;
use App\Http\Requests\Api\GetProviderByIdRequest;
use App\Http\Requests\Api\MakeNewOrderRequest;
use App\Http\Resources\OrderResource;
use App\Http\Resources\ProviderResource;
use App\Http\Resources\SettingResource;
use App\Models\Contact;
use App\Models\Order;
use App\Models\Provider;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Models\Service;
use App\Http\Resources\CategoryResource;

class SettingController extends Controller
{
    public function about()
    {
        return apiResponse(new SettingResource(Setting::where('key', 'about_app')->first()), 'success');
    }

    public function privacy()
    {
        return apiResponse(new SettingResource(Setting::where('key', 'use_policy')->first()), 'success');
    }

    public function contact(contactUsRequest $request)
    {
        return Contact::apiCreateContact($request);
    }

    public function contactus()
    {
        return apiResponse(SettingResource::collection(Setting::getContactUs()->get()),'success');
    }
}
