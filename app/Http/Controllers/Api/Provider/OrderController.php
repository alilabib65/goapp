<?php

namespace App\Http\Controllers\Api\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\orderAcceptOrRefuseRequest;
use App\Http\Resources\NotificationResource;
use App\Http\Resources\OrderResource;
use App\Http\Resources\UserOrderResource;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function orders()
    {
        return apiResponse(new UserOrderResource(auth()->guard('provider')->user()->orders), 'success');
    }

    public function notifications()
    {
        return apiResponse(NotificationResource::collection(auth()->guard('provider')->user()->notifications),'success');
    }

    public function getOrder($order_id)
    {
        if(!$order = Order::find($order_id)) return response()->json(['data' => [], 'message' => 'عفوا هذا الطلب غير موجود', 'status' => false], 422);

        return apiResponse(new OrderResource($order),'success');
    }

    public function orderAcceptOrRefuse(orderAcceptOrRefuseRequest $request)
    {
        return Order::apiOrderAcceptOrRefuse($request);
    }

    public function getProviderHome()
    {
        return apiResponse(OrderResource::collection(auth()->guard('provider')->user()->orders),'success');
    }
}
