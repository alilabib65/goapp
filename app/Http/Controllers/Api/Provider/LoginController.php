<?php

namespace App\Http\Controllers\Api\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ProviderForgetPasswordRequest;
use App\Http\Requests\Api\ProviderLoginRequest;
use App\Http\Requests\Api\ProviderRegisterRequest;
use App\Models\Provider;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(ProviderLoginRequest $request)
    {
        return Provider::apiProviderLogin($request);
    }

    public function register(ProviderRegisterRequest $request)
    {
        return Provider::apiProviderRegister($request);
    }

    public function forgetPassword(ProviderForgetPasswordRequest $request)
    {
        return Provider::apiForgetPassword($request);
    }
}
