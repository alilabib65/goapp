<?php

namespace App\Http\Controllers\Api\Provider;

use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationResource;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function getProviderNotifications()
    {
        return apiResponse(NotificationResource::collection(auth()->guard('provider')->user()->notifications),'success');
    }
}
