<?php

namespace App\Http\Controllers\Api\Provider;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Models\Service;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function subCategories()
    {
        if(!$categories = Service::where('parent_id','!=',null)->get()) return apiResponse([], 'success', null, trans('لايوجد قسام متاحة'), 503);

        return apiResponse(CategoryResource::collection($categories),'success', null, trans('اقسام الخدمات'), 200);
    }
}
