<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    //

     use AuthenticatesUsers;


    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login()
    {
        return view('dashboard.auth.login');
    }


   public function auth(Request $request)
    {
        if (auth()->attempt(['email' => $request->email, 'password' => $request->password], true)) {
            return redirect()->route('admin_dashboard.index')->withTrue('مرحبا بك مره آخري');
        }else{
            return redirect()->route('login')->withFalse('بيانات غير صحيحه');
        }
    }

   public function logout()
    {
        Auth::guard('web')->logout();
        return redirect()->route('login')->withTrue('تم تسجيل الخروج');
    }

}
