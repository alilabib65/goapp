<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Service;
use App\Http\Requests\dashboard\SubServiceRequest;

class SubServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Service::where('parent_id','!=',null)->paginate(10);
        return view('dashboard.subservices.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Service::where('parent_id',null)->get();
        return view('dashboard.subservices.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubServiceRequest $request)
    {
        $service            = new Service();
        $service->name      = $request->name;
        $service->desc      = $request->desc;
        $service->parent_id = $request->parent_id;
        if ($request->image) {
        $service->img       = uploadImg($request->image);
        }
        $service->save();

        return redirect()->back()->withTrue('تم الإضافة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $subservice)
    {
      $categories = Service::where('parent_id',null)->get();

      return view('dashboard.subservices.edit',compact('subservice','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubServiceRequest $request, Service $subservice)
    {
        
        $subservice->name = $request->name;
        $subservice->desc = $request->desc;
        $subservice->parent_id = $request->parent_id;
        if($request->image){
         $subservice->img  = uploadImg($request->image);
        }
        $subservice->save();
        return redirect()->back()->withTrue('تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $subservice)
    {
        $subservice->delete();
        return redirect()->back()->withTrue('تم الحذف بنجاح');
    }
}
