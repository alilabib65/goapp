<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Provider;
use App\Models\Service;
use App\Models\User;
use App\Models\Order;
use Carbon\Carbon;
use App\Http\Requests\dashboard\OrderRequest;

class OrderController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Order::paginate(10);
        return view('dashboard.orders.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Service::where('parent_id','!=',null)->get();
        $users = User::all();
        $providers = Provider::all();
        return view('dashboard.orders.create',compact('categories','users','providers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $order                = new Order();
        $order->provider_id   = $request->provider_id;
        $provider= Provider::find($request->provider_id);
        $order->user_id       = $request->user_id;
        $order->order_lat     = $request->order_lat;
        $order->order_lng     = $request->order_lng;
        $order->service_id    = $provider->service_id;
        $order->receive_lat   = $request->receive_lat;
        $order->receive_lng   = $request->receive_lng;
        $order->details       = $request->details;
        $order->status        = 'wait';
        $order->wait_date     = Carbon::now();
        $order->save();

        return redirect()->back()->withTrue('تم الإضافة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
       $categories = Service::where('parent_id','!=',null)->get();
       $users = User::all();
       $providers = Provider::all();
      return view('dashboard.orders.edit',compact('order','categories','providers','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, Order $order)
    {
        $order->provider_id   = $request->provider_id;
        $provider             = Provider::find($request->provider_id);
        $order->user_id       = $request->user_id;
        $order->order_lat     = $request->order_lat;
        $order->order_lng     = $request->order_lng;
        $order->service_id    = $provider->service_id;
        $order->receive_lat   = $request->receive_lat;
        $order->receive_lng   = $request->receive_lng;
        $order->details       = $request->details;
        $order->save();
        return redirect()->back()->withTrue('تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return redirect()->back()->withTrue('تم الحذف بنجاح');
    }

    public function changeStatus()
    {

    }
}
