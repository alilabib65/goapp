<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Setting,Admin};
use App\Http\Requests\dashboard\{SettingRequest,ProfileRequest};
use Auth;
class SettingController extends Controller
{
    public function index()
    {
        return view('dashboard.setting.index');
    }


    public function store(SettingRequest $request)
    {
       if (!request()->ajax()) {
          if (Auth::check()) {
              $inputs=$request->except('logo','password','_token');
              if ($request->password) {
                $inputs['password']=$request->password;
              }
              if ($request->hasFile('logo')) {
                  $setting=Setting::where('key','logo')->first();
                  $setting=($setting && $setting->value) ? $setting : null;
                  $inputs['logo']=uploadImg($request->logo);
              }
              foreach ($inputs as $key => $value) {
                  Setting::updateOrCreate(['key' => trim($key)],['value'=> $value]);
              }
               return redirect()->back()->withTrue('تم التعديل بنجاح');
        }else {
          abort(404);
        }
      }

    }

    public function profile()
    {
        $user = auth()->user();
        return view('dashboard.setting.profile',compact('user'));
    }

    public function updateProfile(ProfileRequest $request,Admin $admin)
    {
        $admin->name     = $request->name;
        $admin->email    = $request->email;
        if($request->password){
            $admin->password = $request->password;
        }
        $admin->save();

        return redirect()->back()->withTrue('تم تحديث الحساب بنجاح');
    }
}
