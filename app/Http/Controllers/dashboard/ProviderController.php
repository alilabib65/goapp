<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Models\Provider;
use App\Models\Service;
use App\Http\Requests\dashboard\ProviderRequest;
use App\Http\Requests\dashboard\EditProviderRequest;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $data = Provider::paginate(10);
        return view('dashboard.customers.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $categories = Service::where('parent_id', '!=', null)->get();
        return view('dashboard.customers.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProviderRequest $request
     * @return Response
     */
    public function store(ProviderRequest $request)
    {
        $user = new Provider();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->city = $request->city;
        $user->service_id = $request->service_id;
        $user->identity = $request->identity;
        if ($request->identity_img) {
            $user->identity_img = uploadImg($request->identity_img);
        }
        $user->password = $request->password;
        $user->status = $request->status;
        $user->save();

        return redirect()->back()->withTrue('تم الإضافة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Provider $provider
     * @return Factory|View
     */
    public function edit(Provider $provider)
    {
        $categories = Service::where('parent_id', '!=', null)->get();

        return view('dashboard.customers.edit', compact('provider', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditProviderRequest $request
     * @param Provider $provider
     * @return Response
     */
    public function update(EditProviderRequest $request, Provider $provider)
    {
        $provider->name = $request->name;
        $provider->email = $request->email;
        $provider->mobile = $request->mobile;
        $provider->city = $request->city;
        $provider->service_id = $request->service_id;
        $provider->identity = $request->identity;

        if ($request->identity_img) {
            $provider->img = uploadImg($request->identity_img);
        }
        if ($request->password) {
            $provider->password = $request->password;
        }
        $provider->status = $request->status;
        $provider->save();
        return redirect()->back()->withTrue('تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Provider $provider
     * @return Response
     * @throws Exception
     */
    public function destroy(Provider $provider)
    {
        $provider->delete();
        return redirect()->back()->withTrue('تم الحذف بنجاح');
    }
}
