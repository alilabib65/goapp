<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\Offer;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Requests\dashboard\OfferRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\View\View;

class OffersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        return view('dashboard.offers.index', ['data' => Offer::paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('dashboard.offers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OfferRequest $request
     * @return Response
     */
    public function store(OfferRequest $request)
    {
        $offer = new Offer();
        $offer->name = $request->name;
        $offer->desc = $request->desc;
        if ($request->image) {
            $offer->img = uploadImg($request->image);
        }
        $offer->save();

        return redirect()->back()->withTrue('تم الإضافة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Offer $offer
     * @return Factory|View
     */
    public function edit(Offer $offer)
    {
        return view('dashboard.offers.edit', compact('offer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OfferRequest $request
     * @param Offer $offer
     * @return Response
     */
    public function update(OfferRequest $request, Offer $offer)
    {
        $offer->name = $request->name;
        $offer->desc = $request->desc;
        if ($request->image) {
            $offer->img = uploadImg($request->image);
        }
        $offer->save();
        return redirect()->back()->withTrue('تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Offer $offer
     * @return Response
     * @throws Exception
     */
    public function destroy(Offer $offer)
    {
        File::delete('storage/images/' . $offer->img);

        $offer->delete();

        return redirect()->back()->withTrue('تم الحذف بنجاح');
    }
}
