<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Contact,User,Provider};
use App\Http\Requests\dashboard\ContactRequest;
class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Contact::where('type','recive')->paginate(10);
        return view('dashboard.contact.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $providers = Provider::all();
        return view('dashboard.contact.create',compact('users','providers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request)
    {
        $contact          = new Contact();
        $contact->from    = $request->from;
        if($contact->from =='user'){
          $contact->user_id    = $request->user_id;
        }elseif($contact->from =='provider'){
          $contact->provider_id = $request->provider_id;
        }
        $contact->subject = $request->subject;
        $contact->content = $request->content;
        $contact->type    = 'sent';

        $contact->save();

        return redirect()->back()->withTrue('تم الإضافة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
      return view('dashboard.services.edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactRequest $request, Contact $contact)
    {
        $contact->from    = $request->from;
        if($contact->from =='user'){
          $contact->user_id    = $request->user_id;
        }elseif($contact->from =='provider'){
          $contact->provider_id = $request->provider_id;
        }
        $contact->subject = $request->subject;
        $contact->content = $request->content;
        $contact->type    = 'sent';
        $service->save();
        return redirect()->back()->withTrue('تم التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();
        return redirect()->back()->withTrue('تم الحذف بنجاح');
    }

    public function sent()
    {
        $data = Contact::where('type','sent')->paginate(10);
        return view('dashboard.contact.sent',compact('data'));
    }
}
