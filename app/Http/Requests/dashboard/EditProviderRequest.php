<?php

namespace App\Http\Requests\dashboard;

use Illuminate\Foundation\Http\FormRequest;

class EditProviderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'nullable|string|email',
            'mobile'=>'required',
            'city'=>'nullable',
            'service_id'=>'required',
            'identity'=>'required',
            'identity_img'=>'nullable',
            'status'=>'required'
        ];
    }
}
