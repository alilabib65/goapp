<?php

namespace App\Http\Requests\dashboard;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from'       =>'required',
            'user_id'    =>'required_if:from,user',
            'provider_id'=>'required_if:form,provider',
            'content'    =>'required',
            'subject'    =>'required',
        ];
    }
}
