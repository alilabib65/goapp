<?php

namespace App\Http\Requests\dashboard;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider_id'=>'required',
            'user_id'    =>'required',
            'order_lat'  =>'required',
            'order_lng'  =>'required',
            'receive_lat'=>'required',
            'receive_lng'=>'required',
            'details'    =>'required',
        ];
    }
}
