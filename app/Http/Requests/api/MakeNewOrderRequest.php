<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class MakeNewOrderRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'provider_id' => 'required|exists:providers,id',
            'service_id'  => 'required|exists:services,id',
            'order_lat'   => 'required|numeric',
            'order_lng'   => 'required|numeric',
            'receive_lat' => 'required|numeric',
            'receive_lng' => 'required|numeric',
            'details'     => 'required|string',
        ];
    }
}
