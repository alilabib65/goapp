<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class UserLoginRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'mobile'       => 'required|exists:users,mobile',
            'password'     => 'required|string',
            'device_token' => 'required|string'
        ];
    }
}
