<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class UserRegisterRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'     => 'required|string',
            'mobile'   => 'required|unique:users,mobile',
            'phone'    => 'nullable|unique:users,phone',
            'email'    => 'nullable|unique:users,email',
            'city'     => 'required|string',
            'password' => 'required|string|min:6',
            'device_token' => 'required|string',
        ];
    }
}
