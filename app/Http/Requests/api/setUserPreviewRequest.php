<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class setUserPreviewRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'rate'        => 'required|numeric|in:1,2,3,4,5',
            'provider_id' => 'required|numeric|exists:providers,id',
            'comment'     => 'nullable|string',
        ];
    }
}
