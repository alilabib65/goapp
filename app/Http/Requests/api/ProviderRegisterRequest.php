<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class ProviderRegisterRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'         => 'required|string',
            'mobile'       => 'required|unique:providers,mobile',
            'email'        => 'required|unique:providers,email',
            'service_id'   => 'required|exists:services,id',
            'identity'     => 'required|numeric',
            'lat'          => 'required|numeric',
            'lng'          => 'required|numeric',
            'identity_img' => 'required|mimes:png,jpg,jpeg',
            'city'         => 'required|string',
            'password'     => 'required|string|min:6',
            'device_token' => 'required|string',
        ];
    }
}
