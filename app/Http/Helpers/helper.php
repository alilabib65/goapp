<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

function str(){	return new \Str; }
function arr(){	return new \Illuminate\Support\Arr; }
function carbon(){ return new \Carbon\Carbon; }

function floating($right, $left){ return app()->isLocale('ar') ? $right : $left;}
function getPrice($price){ return $price . ' ' .trans('back.reyal');}
function translated($type, $obj){ return trans('back.' . $type . '-done', ['var' => trans('back.t-' . $obj)]);}

function cities(){ return city()->whereStatus(1)->get();}
function check_if_message_or_image($message){ return (explode('message_', $message)[0] == '') ? getImage('messages',$message) : $message;}

function str_limit_30($text){ return ucwords(str()->limit($text, 30, '...')); }
function isNullable($text){	return (isset($text) || $text != null || $text != '') ? ucwords($text) : trans('back.no-value'); }
function getTime($time){ return carbon()->createFromFormat('H:i', $time)->format('h:i A'); }
function create_rand_numbers($digits = 4){ return rand(pow(10, $digits-1), pow(10, $digits)-1); }
function dateIsToday($model){ return $model->created_at->isToday(); }
function setActive($url) { return Request::is($url) ? 'active' : ''; }

function getSetting($setting_name)
{
	$setting = App\Models\Setting::whereStatus(1)->where('key', $setting_name)->first();
	return $setting ? $setting->value : trans('back.no-value');
}

function uploaded($img, $model)
{
    $filename =  $model . '_' . str()->random(12) . '_' . date('Y-m-d') . '.' . strtolower($img->getClientOriginalExtension());

    if (!file_exists(public_path('uploaded/'.str()->plural($model).'/')))
        mkdir(public_path('uploaded/'.str()->plural($model).'/'), 0777, true);

    $path = public_path('uploaded/'.str()->plural($model).'/');

    Image::make($img)->save($path . $filename);

    return $filename;
}

function uploadPDFOrRecord($request, $filename, $type)
{
    $name = null;

    if(Input::hasFile($filename))
    {
        $ex = Input::file($filename)->getClientOriginalExtension();

        $name = $type . '_' . str()->random(12) . '_' . date('Y-m-d') .'.'. $ex;

        Input::file($filename)->move('public'.DIRECTORY_SEPARATOR.'uploaded'.DIRECTORY_SEPARATOR.'files', $name);
    }

    return $name;
}

function GetDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
{
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);

    $latDelta = $latTo - $latFrom;
    $lonDelta = $lonTo - $lonFrom;

    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
    $x = ($angle * $earthRadius) / 1000;
    return (int) $x;
}

function upload_file_by_type($request)
{
    if($request->type == 'video') return $request->video;

    elseif ($request->type == 'pdf') return uploadPDFOrRecord($request, 'pdf', 'pdf');

    elseif ($request->type == 'record') return uploadPDFOrRecord($request, 'record', 'record');

    else return null;
}

function updateStatus($data, $model)
{
    $value = $data['value'];

    // if the value comes with checked that mean we want the reverse value of it;
    return ($value == 'checked') ? $model->update(['status' => 0]) : $model->update(['status' => 1]);
}

function permission_checker($auth)
{
    $permissions = $auth->role->permissions;

    foreach($permissions as $permission)
    {
        $name = $permission->permissions;

        $currentRoute = request()->route()->getName();

        if($currentRoute != $name) continue;

        return true;
    }
    return false;
}

function userActivity($user, $type = 'create')
{
    switch ($type)
    {
        case 'update':
            $message_log = 'قام العضو ' . $user->name . ' بتحديث حسابه';
            break;

        case 'delete':
            $message_log = 'قام العضو ' . $user->name . 'بإلغاء حسابه';
            break;

        default:
            $message_log = 'قام العضو ' . $user->name . ' بإنشاء حساب لنفسه';
            break;
    }

    activity('user')->performedOn($user)->causedBy($user->id)->withProperties($user)->log($message_log);

    return true;
}

function apiResponse($data, $type, $pagination = null, $msg = '', $code = 200)
{
    if($type == 'success')
    {
        $response = ['data' => $data,'status' => true];

        $response['message'] = ($msg == '') ? trans('api.request-done-successfully') : $msg;

        if($pagination)
        {
            $response['last_page']    = $pagination['last_page'];
            $response['perPage']      = $pagination['perPage'];
            $response['current_page'] = $pagination['current_page'];
        }

        return response()->json($response, $code);
    }

    $response = ['data' => $data,'status' => false];

    $response['message'] = ($msg == '') ? trans('api.server_internal_error') : $msg;

    return response()->json($response, 500);
}

function app_commission_for_user($user_id)
{
	$user = App\Models\User::find($user_id);

	if($user->advertisements)
	{
		$ads_costs = $user->advertisements->map(function($ad){ return $ad->advertisement->price; })->toArray();
	}
	else
	{
		$ads_costs = [0];
	}


	$percent = ((int)getSetting('app_tax') / 100);

	return round((int)array_sum($ads_costs) * $percent);
}

if (!function_exists('is_url'))
{
	function is_url($url)
	{
		$regex = "((https?|ftp)\:\/\/)?"; // SCHEME
	    $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
	    $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP
	    $regex .= "(\:[0-9]{2,5})?"; // Port
	    $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path
	    $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
	    $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor

	    // `i` flag for case-insensitive
       	return (preg_match("/^$regex$/i", $url)) ? true : false;
	}
}

function getAdditionalImages($path, $_images)
{
    $images = [];

    if($_images) {
	    foreach($_images as $image) { $images[$image->id] = getImage($path, $image->image_name); }
    }

    return empty($images) ? asset('public/admin/assets/images/placeholder.jpg') : $images;
}

function getList($list) { return count($list) > 0 ? ['' => trans('back.select-a-value')] + $list : [0 => trans('back.no-value')]; }

function setImageSrc($model, $path)
{
	if($model != null){ if($model->image) return getImage($path, $model->image); }
	return '';
}

function models($withColors = false)
{
	if ($withColors) {
		return [
			'teal'   => 'admin',
			'pink'   => 'user',
			'blue'   => 'city',
		];
	}
	// those models for CRUD stystem only;
	return [
		'user-tie'     => 'admin',
		'users'        => 'user',
		'user'         => 'teacher',
		'city'         => 'city',
		'gear'         => 'setting',
		'list'         => 'category',
		'file-upload2' => 'upload',
	];
}

function convert2english($string) {
    $newNumbers = range(0, 9);
    $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
    $string =  str_replace($arabic, $newNumbers, $string);
    return $string;
}

function getModelCount($model, $withDeleted = false)
{
	if ($withDeleted) {
		if ($model == 'admin') return \App\Models\Admin::onlyTrashed()->where('is_super_Admin', '!=', 1)->count();

		$mo = "App\\Models\\" . ucwords($model);

		return $mo::onlyTrashed()->count();
	}

	if ($model == 'admin') return \App\Models\Admin::where('is_super_Admin', '!=', 1)->count();
	if ($model == 'teacher') return App\Models\User::where('type', 'teacher')->count();
	if ($model == 'user') return App\Models\User::where('type', 'student')->count();

	$mo = "App\\Models\\" . ucwords($model);

	return $mo::count();
}

function save_translated_attrs($model, $formTranslatedAttrs, $otherAttrs = null, $excepted = [])
{
	foreach (config('sitelangs.locales') as $lang => $name) {
		foreach ($model->translatedAttributes as $attr) {
			$model->translateOrNew($lang)->$attr = $formTranslatedAttrs[$lang][$attr];
		}
	}

	if ($otherAttrs != null) {
		$otherAttrs = array_except($otherAttrs, ['_method', '_token'] + $excepted);
		foreach ($otherAttrs as $key => $value) {
			$model->$key = $value;
		}
	}
}

function getCategoryByType($type = 0)
{
	$_categories = \App\Models\Service::where('parent_id', $type)->get();

	$categories = array();

	foreach ($_categories as $key => $category) { $categories[$category->id] = $category->name; }

	return $categories;
}

function get_path_by_file_type($file)
{
    if($file->type == 'pdf' || $file->type == 'record') return url('public/uploaded/files/' . $file->file);

    else return $file->file;
}

function load_translated_attrs($model)
{
	foreach (config('sitelangs.locales') as $lang => $name) {
		$model->$lang = [];
		foreach ($model->translatedAttributes as $attr) $model->$lang = $model->$lang + [$attr => $model->getTranslation($lang)->$attr];
	}
}

function pagination($page, $model, $perPage = 10)
{
	$start  = ($page == 1) ? 0 : ($page - 1) * $perPage;

	$paginatedModel = ($page == 0 ? $model : $model->slice($start, $perPage));

	$total = $model->count();

	$count_pages = (int) $total / (int) $perPage;

	$last_page =  $count_pages >= 1 ? (int) $count_pages : 1;

	$data['total'] 	   = $total;
	$data['paginated'] = $paginatedModel;
	$data['last_page'] = $last_page;
	$data['perPage']   = $perPage;

	return $data;
}

function push_notification($title, $body, $fcm_token)
{
	//send notify
	$fcmNotification['notification']['title'] = $title;
	$fcmNotification['notification']['body']  = $body;

	$fcmNotification['priority'] 			  = "high";

	$fcmNotification['data']['click_action'] = "FLUTTER_NOTIFICATION_CLICK";
	$fcmNotification['data']['title'] 		 = $title;
	$fcmNotification['data']['body'] 		 = $body;

	$fcmNotification['to']                   = $fcm_token;

	create_curl_notification($fcmNotification);

	return true;
}

function create_curl_notification($fcmNotification = [])
{
	$fcmUrl = 'https://fcm.googleapis.com/fcm/send';

	$fcmNotification['notification']['sound'] = false;

	$key = Setting('notification_key');

	$headers = ['Authorization: key=' . $key, 'Content-Type: application/json'];

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $fcmUrl);

	curl_setopt($ch, CURLOPT_POST, true);

	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));

	$result = curl_exec($ch);

	$response = json_decode($result);

	$res = ($response != null ? true : false);

	curl_close($ch);

	return $res;
}

function payTabsPayment($payment, $request)
{
	$url = 'https://www.paytabs.com/apiv2/verify_payment_transaction';

	$headers = ['content-type: multipart/form-data'];

	$data['merchant_email'] = $payment['email'];
	$data['secret_key']     = $payment['key'];
	$data['transaction_id'] = $request->transaction_id;
	$data['order_id']       = $request->order_id;

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	$result = curl_exec($ch);

	$response = json_decode($result);

	$_res = [];

	switch ($response->response_code) {
	    case 4001:
	        $msg = 'Missing parameters.';
	        break;

	    case 4002:
	        $msg = 'Invalid Credentials.';
	        break;

	    case 4003:
	        $msg = 'There are no transactions available.';
	        break;

	    case 0404:
	        $msg = 'You don’t have permissions.';
	        break;

	    case 481:
	        $msg = 'This transaction may be suspicious, your bank holds for further confirmation. Payment Provider has rejected this transaction due to suspicious activity; Your bank will reverse the dedicated amount to your card as per their policy.';
	        break;

	    default:
	        $msg = 'Payment is completed Successfully.';
	        $_res = $response;
	        break;
	}

	$res['msg'] = $msg;
	$res['code'] = $response->response_code;
	$res['result'] = $_res;

	curl_close($ch);

	return $res;
}
