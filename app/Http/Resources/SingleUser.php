<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SingleUser extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'     => $this->id,
            'name'   => $this->name ?? '',
            'mobile' => $this->mobile ?? '',
            'phone'  => $this->phone ?? '',
            'email'  => $this->email ?? '',
            'city'   => $this->city ?? '',
            'token'  => $this->token->jwt ?? ''
        ];
    }
}
