<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'              => $this->id,
            'service'         => new CategoryResource($this->category),
            'provider'        => new ProviderResource($this->provider),
            'order_lat'       => $this->order_lat ?? '',
            'order_lng'       => $this->order_lng ?? '',
            'receive_lat'     => $this->receive_lat ?? '',
            'receive_lng'     => $this->receive_lng ?? '',
            'details'         => $this->details ?? '',
            'status'          => $this->status ?? '',
            'distance'        => GetDistance($this->order_lat,$this->order_lng,$this->provider->lat,$this->provider->lng),
        ];
    }
}
