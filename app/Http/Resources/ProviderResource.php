<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProviderResource extends JsonResource
{
    public function toArray($request)
    {
        $dis = $request->has(['lat', 'lng']) ? GetDistance($this->lat, $this->lng, $request->lat, $request->lng) : 0;

        return [
            'id'           => $this->id,
            'name'         => $this->name   ?? '',
            'email'        => $this->email  ?? '',
            'mobile'       => $this->mobile ?? '',
            'city'         => $this->city   ?? '',
            'rate'         => $this->rate   ?? '0',
            'lat'          => $this->lat    ?? '',
            'lng'          => $this->lng    ?? '',
            'distance'     => $dis,
            'identity'     => $this->identity         ?? '',
            'identity_img' => $this->identity_img_url ?? '',
            'token'        => $this->token->jwt       ?? '',
            'service'      => new ServiceResource($this->service),
        ];
    }
}
