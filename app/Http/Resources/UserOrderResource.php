<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserOrderResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'waited' => OrderResource::collection($this->where('status', '!=', 'done')),
            'done'   => OrderResource::collection($this->where('status', 'done')),
        ];
    }
}
