<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OfferResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'   => $this->id,
            'name' => $this->name ?? '',
            'desc' => $this->desc ?? '',
            'img'  => $this->image_url,
        ];
    }
}
