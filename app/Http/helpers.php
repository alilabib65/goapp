<?php
// namespace App\Http;
use Illuminate\Support\Str;
use App\Notifications\General\GeneralNotification;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\File as File;
use Illuminate\Support\Facades\Notification as Notification;
use LaravelFCM\Facades\FCM as FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\Models\{UserProfile,
    Device,
    User,
    Role,
    Permission,
    Entity,
    ReceptionDesk,
    Warehouse,
    Attendance,
    LessonAttendResult,
    VehicleChair,
    PointSetting,
    Setting,
    StudentVote};
use Carbon\Carbon;

//return Settings
function setting($attr)
{
    if (\Schema::hasTable('settings')) {
        $setting = Setting::where('key', $attr)->first() ?? [];
        if ($attr == 'project_name') {
            return !empty($setting) ? $setting->value : trans('dashboard.setting.default_name');
        }
        if ($attr == 'logo') {
            return !empty($setting) ? $setting->value : '/panelAsset/assets/global/images/logo_icon_dark.png';
        }
        if (!empty($setting)) {
            return $setting->value;
        }
        return false;
    }
    return false;
}


function sendNotify($user)
{
    try {
        if ($user->roles()->exists()) {
            $user->notify(new RegisterUser($user));
        } else {
            $user->notify(new VerifyApiMail($user));
        }
        $msg = [trans('dashboard.public.success_add_send'), 1];
    } catch (\Exception $e) {
        $msg = [trans('dashboard.public.success_add_not_send'), 0];
    }
    return $msg;
}

/**
 * If User Out Of System
 *
 * @param  $outSystem
 * @param  $method
 * @param  $dataOfCommunicate
 */
function sendNotification($users, $dataArray, $outSystem = null, $method = null, $dataOfCommunticate = null)
{
    if ($outSystem) {
        Notification::route($method, $dataOfCommunticate)
            ->notify(new GeneralNotification($dataArray));
    } else {
        Notification::send($users, new GeneralNotification($dataArray));
    }
}

function uploadImg($files, $url = 'images', $key = 'image', $model = null, $width = null, $height = null)
{
    $dist = storage_path('app/public/' . $url . "/");
    $image = $dist . 'photo.jpg';
    if (!is_array($files)) {
        $dim = getimagesize($files);
        $width = $width ?? $dim[0];
        $height = $height ?? $dim[1];
    }
    if ($model && $model->$field != $dist . 'photo.jpg') {
        File::delete($model->$field);
    }
    if (gettype($files) == 'array') {
        $image = [];
        foreach ($files as $img) {
            $dim = getimagesize($img);
            $width = $width ?? $dim[0];
            $height = $height ?? $dim[1];
            if ($img) {
                Image::make($img)->resize($width, $height, function ($cons) {
                    $cons->aspectRatio();
                })->save($dist . $img->hashName());
                $image[][$key] = $img->hashName();
            }
        }
    } else {
        Image::make($files)->resize($width, $height, function ($cons) {
            $cons->aspectRatio();
        })->save($dist . $files->hashName());
        $image = $files->hashName();
    }
    return $image;
}

function uploadFile($files, $url = 'files', $key = 'file', $model = null)
{
    $dist = storage_path('app/public/' . $url);
    $file = '';
    if ($model && $model->$field != $dist . 'photo.jpg') {
        File::delete($model->$field);
    }
    if (gettype($files) == 'array') {
        $file = [];
        foreach ($files as $new_file) {

            $file_name = uniqid() . "&&file_" . $new_file->getClientOriginalName();
            if ($new_file->move($dist, $file_name)) {
                $file[][$key] = $file_name;
            }
        }
    } else {
        $file = $files;
        $file_name = time() . "&&file_" . $file->getClientOriginalName();
        if ($file->move($dist, $file_name)) {
            $file = $file_name;
        }
    }

    return $file;
}

function validate_phone_number($phone)
{
    // Allow +, - and . in phone number
    $filtered_phone_number = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
    // Remove "-" from number
    $phone_to_check = str_replace("-", "", $filtered_phone_number);
    // Check the lenght of number
    // This can be customized if you want phone number from a specific country
    // if (strlen($phone_to_check) < 10 || strlen($phone_to_check) > 14) {
    //    return false;
    // } else {
    //   return true;
    // }
    return $phone_to_check;
}

/**
 * Push Notifications to phone FCM
 *
 * @param array $fcmData
 * @param array $userIds
 */
function pushFcmNotes($fcmData, $userIds)
{
    $send_process = [];
    $fail_process = [];
    if (is_array($userIds) && !empty($userIds)) {
        $devices = Device::whereIn('user_id', $userIds)->pluck('device_token')->toArray();
        if (count($devices)) {
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);

            $notificationBuilder = new PayloadNotificationBuilder($fcmData['title']);
            $notificationBuilder->setBody($fcmData['body'])
                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData($fcmData);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            // You must change it to get your tokens
            $downstreamResponse = FCM::sendTo($devices, $option, $notification, $data);
            // return $downstreamResponse;
            return $downstreamResponse->numberSuccess();
        }
        return 0;
    }
    return "No Users";
}

// HISMS
function send_sms($mobile, $msg)
{
    $sender_mobile = setting('hisms_phone');
    $sender_name = str_replace(' ', '%20', setting('site_name'));
    $password = setting('hisms_password');
    $numbers = $mobile;
    $msg = str_replace(' ', '%20', $msg);
    $date = date('Y-m-d');
    $time = date("H:i");
    $url = "http://hisms.ws/api.php?send_sms&username=" . $sender_mobile . "&password=" . $password . "&numbers=" . $numbers . "&sender=" . $sender_name . "&message=" . $msg . "&date=" . $date . "&time=" . $time;
    $msg = [];
    $response = (int)file_get_contents($url);
    $result = validate_response($response);
    return $msg = ['response' => $response, 'result' => $result];
}

function validate_response($response)
{
    $result = '';
    switch ($response) {
        case 1:
            $result = 'اسم المستخدم غير صحيح';
            break;
        case 2:
            $result = 'كلمة المرور غير صحيحة';
            break;
        case 3:
            $result = 'تم ارسال كود التحقق';
            break;
        case 4:
            $result = 'لايوجد ارقام';
            break;
        case 5:
            $result = 'لايوجد رسالة';
            break;
        case 6:
            $result = 'رقم الهاتف غير صحيح';
            break;
        case 7:
            $result = 'رقم الهاتف غير مفعل';
            break;
        case 8:
            $result = 'الرسالة تحتوى على كلمة ممنوعة';
            break;
        case 9:
            $result = 'لايوجد رصيد';
            break;
        case 10:
            $result = 'صيغة التاريخ غير صحيحة';
            break;
        case 10:
            $result = 'صيغة الوقت غير صحيحة';
            break;
        case 404:
            $result = 'لم يتم ادخال جميع البرمترات المطلوبة';
            break;
        case 403:
            $result = 'تم تجاوز عدد المحاولات المسموحة';
            break;
        case 504:
            $result = 'الحساب معطل';
            break;
    }
    return $result;
}

function userProfile($user_id = 0)
{
    $userProfile = UserProfile::where('user_id', $user_id)->first();
    if ($userProfile) {
        return $userProfile;
    }
    return false;
}

function getUser($user_id = 0)
{
    $user = User::find($user_id);
    if ($user) {
        return $user;
    }
    return false;
}

function Role()
{
    $role = Role::find(Auth::user());
    if ($role) {
        return $role;
    }
}


function currentRoute()
{
    $routes = Route::getRoutes();
    foreach ($routes as $value) {
        if ($value->getName() === Route::currentRouteName()) {
            echo $value->getAction()['title'];
        }
    }
}

// start orabi helpers
function random_colors()
{
    $color_array = [
        'slate-300', 'grey-300', 'brown-300', 'green-600', 'brown-600',
        'orange-300', 'orange-700', 'slate-700', 'green-300', 'teal-300',
        'blue-300', 'green-800', 'blue-600', 'blue-800', 'indigo-300', 'indigo-700',
        'purple-300', 'purple-600', 'violet-300', 'violet-600', 'pink-300', 'pink-600',
        'info-300', 'info-600', 'info-800', 'danger-300', 'danger-600'
    ];
    return $color_array[array_rand($color_array)];
}

// end orabi helpers

// feisal helpers

//============= for youtube ============
function url_exists($url)
{
    $headers = get_headers($url);
    return stripos($headers[0], "200 OK") ? true : false;
}

function get_youtube_id($url)
{
    $youtubeid = explode('v=', $url);
    if (isset($youtubeid[1])) {
        $youtubeid = explode('&', $youtubeid[1]);
        $youtubeid = $youtubeid[0];
        return $youtubeid;
    }
    return 0;
}

function get_youtube_thumb($id)
{
    if (url_exists('http://img.youtube.com/vi/' . $id . '/sddefault.jpg')) {
        $image = 'http://img.youtube.com/vi/' . $id . '/sddefault.jpg';
    } elseif (url_exists('http://img.youtube.com/vi/' . $id . '/mqdefault.jpg')) {
        $image = 'http://img.youtube.com/vi/' . $id . '/mqdefault.jpg';
    } elseif (url_exists('http://img.youtube.com/vi/' . $id . '/maxresdefault.jpg')) {
        $image = 'http://img.youtube.com/vi/' . $id . '/maxresdefault.jpg';
    } else {
        $image = false;
    }
    return $image;
}

function getExelData($file, $arr)
{
    $extension = \File::extension($file->getClientOriginalName());
    if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
        $fileName = time() . rand(11111, 99999) . '.' . $extension;

        $dest = storage_path('app/public/' . 'files' . "/");
        $file->move($dest, $fileName);
        $path = $dest . '/' . $fileName;

        $data = Excel::import(new \App\Imports\StudentImport($arr), $path);

        return $data;
    }
}

// end feisal helpers

// function menu()
// {
//   $routes = Route::getRoutes();
//   foreach ($routes as $value)
//   {
//     if($value->getName() !== null)
//     {
//       if(isset($value->getAction()['title']) && !isset($value->getAction()['front']) && isset($value->getAction()['icon']))
//       {
//         if(isset($value->getAction()['child']) && isset($value->getAction()['subTitle']) && isset($value->getAction()['subIcon']))
//         {
//                     $arr = [];
//                     $permission = Permission::where('role_id',Auth::user()->role)->select('permissions')->get();
//                     foreach($permission as $key=>$per)
//                     {
//                         $arr[$key] = $per->permissions;
//                     }

//                     if(in_array($value->getName(),$arr))
//                     {
//                         echo '<li>';
//                         echo '<a class="drop-down-btn">'.$value->getAction()['icon'].$value->getAction()['title']. '</a>';
//                             echo '<ul class="drop-down-menu">';
//                             echo '<li><a class="drop-down-btn" href="'.route($value->getName()).'">'.$value->getAction()['subTitle']. $value->getAction()['subIcon'].'</a></li>';
//                                 foreach ($value->getAction()['child'] as $child)
//                                 {
//                                     #foreach for sub links
//                                     $routes = Route::getRoutes();
//                                     foreach ($routes as $value)
//                                     {
//                                         if($value->getName() !== null && isset($value->getAction()['icon']))
//                                         {
//                                             if($value->getName() == $child)
//                                             {
//                                                 if(in_array($value->getName(),$arr))
//                                                 {
//                                                     echo '<li><a href="'.route($value->getName()).'">'.$value->getAction()['title'].$value->getAction()['icon'].'</a></li>';
//                                                 }
//                                             }
//                                         }
//                                     }
//                                 }
//                             echo '</ul>';
//                         echo '</li>';
//                     }

//         }else if(isset($value->getAction()['child']) && isset($value->getAction()['icon'])){
//                     if(in_array($value->getName(),$arr))
//                     {
//                         echo '<li><a href="'.route($value->getName()).'">'.$value->getAction()['title'].$value->getAction()['icon'].'</a></li>';
//                     }
//                 }else if(!isset($value->getAction()['child']) && isset($value->getAction()['icon']) && !isset($value->getAction()['hasFather']))
//                 {
//                     $arr = [];
//                     $permission = Permission::where('role_id',Auth::user()->role)->select('permissions')->get();
//                     foreach($permission as $key=>$per)
//                     {
//                         $arr[$key] = $per->permissions;
//                     }
//                     if(in_array($value->getName(),$arr))
//                     {
//                         echo '<li><a href="'.route($value->getName()).'">'.$value->getAction()['title'].$value->getAction()['icon'].'</a></li>';
//                     }
//                 }
//       }
//     }
//   }
// }

// function Permissions(){
//   $routes = Route::getRoutes();
//   foreach ($routes as $value)
//   {
//     if($value->getName() !== null)
//     {
//       if(isset($value->getAction()['title']) && !isset($value->getAction()['front']) && isset($value->getAction()['child']))
//       {
//                 echo '<div class="col-sm-2" style="border: 1px solid #000;margin-right:10px;margin-bottom:5px;padding:0">';
//                 foreach ($value->getAction()['child'] as $child)
//                 {


//                     if(isset($value->getAction()['title']))
//                     {
//                         echo '<label class="checkbox" style="display:block;background:#37474f;margin-top:0;color:#fff">';
//                         echo '<input type="checkbox" name="permissions[]"  value="'.$value->getName().'"> ';
//                         echo '<i class="icon-checkbox"></i>';
//                         echo '<label class="checkbox" style="padding-right:8px">'.$value->getAction()['title'].'</label>';
//                         echo '</label>';
//                     }

//                     #????
//                     if(isset($value->getAction()['subTitle']))
//                     {
//                         echo '<label class="checkbox" style="display:block;background:#37474f;margin-top:0;color:#fff">';
//                         echo '<input type="checkbox" name="permissions[]"  value="'.$value->getName().'"> ';
//                         echo '<i class="icon-checkbox"></i>';
//                         echo '<label class="checkbox" style="padding-right:8px">'.$value->getAction()['subTitle'].'</label><br>';
//                         echo '</label>';
//                     }

//                     #foreach for sub links
//                     $routes = Route::getRoutes();
//                     foreach ($routes as $value)
//                     {
//                         if($value->getName() !== null && !isset($value->getAction()['icon']) || isset($value->getAction()['hasFather']))
//                         {

//                             if($value->getName() == $child)
//                             {
//                                 echo '<label class="checkbox" style="display:block">';
//                                 echo '<input type="checkbox" name="permissions[]"  value="'.$value->getName().'"> ';
//                                 echo '<i class="icon-checkbox"></i>';
//                                 echo '<label style="padding-right:8px">'.$value->getAction()['title'].'</label><br>';
//                                 echo '</label>';
//                             }
//                         }
//                     }

//                 }
//                 echo '</div>';
//             }
//             if(!isset($value->getAction()['child']) && isset($value->getAction()['icon']) && !isset($value->getAction()['front']) && isset($value->getAction()['title']) && !isset($value->getAction()['hasFather']))
//             {
//                 echo '<div class="col-sm-2" style="border: 1px solid #000;margin-right:10px;margin-bottom:5px;padding:0">';
//                 echo '<label class="checkbox" style="display:block;background:#37474f;margin-top:0;margin-bottom:0;color:#fff">';
//                 echo '<input type="checkbox" name="permissions[]" value="'.$value->getName().'"> ';
//                 echo '<i class="icon-checkbox"></i>';
//                 echo '<label style="padding-right:8px ;margin-top:6px">'.$value->getAction()['title'].'</label><br>';
//                 echo '</label>';
//                 echo '</div>';
//             }
//         }

//   }
// }

// function EditPermissions($id){
//   $routes = Route::getRoutes();
//   foreach ($routes as $value)
//   {
//     if($value->getName() !== null)
//     {
//       if(isset($value->getAction()['title']) && !isset($value->getAction()['front']) && isset($value->getAction()['child']))
//       {

//                 $arr = [];
//                 $permission = Permission::where('role_id',$id)->select('permissions')->get();
//                 foreach($permission as $key=>$per)
//                 {
//                     $arr[$key] = $per->permissions;
//                 }


//                 echo '<div class="col-sm-2" style="border: 1px solid #000;margin-right:7px;margin-bottom:5px;padding:0">';
//                 foreach ($value->getAction()['child'] as $child)
//                 {

//                     if(isset($value->getAction()['title']))
//                     {
//                         if(in_array($value->getName(),$arr))
//                         {
//                             echo '<label class="checkbox" style="display:block;background:#37474f;margin-top:0;color:#fff">';
//                             echo '<input type="checkbox" name="permissions[]" checked value="'.$value->getName().'"> ';
//                             echo '<i class="icon-checkbox"></i>';
//                             echo '<label class="checkbox" style="padding-right:8px">'.$value->getAction()['title'].'</label>';
//                             echo '</label>';
//                         }else
//                         {
//                             echo '<label class="checkbox" style="display:block;background:#37474f;margin-top:0;color:#fff">';
//                             echo '<input type="checkbox" name="permissions[]" value="'.$value->getName().'"> ';
//                             echo '<i class="icon-checkbox"></i>';
//                             echo '<label class="checkbox" style="padding-right:8px">'.$value->getAction()['title'].'</label>';
//                             echo '</label>';
//                         }
//                     }

//                     #????
//                     if(isset($value->getAction()['subTitle']))
//                     {
//                         if(in_array($value->getName(),$arr))
//                         {
//                             echo '<label class="checkbox" style="display:block;background:#37474f;margin-top:0;color:#fff">';
//                             echo '<input type="checkbox" name="permissions[]" checked value="'.$value->getName().'"> ';
//                             echo '<i class="icon-checkbox"></i>';
//                             echo '<label style="padding-right:8px">'.$value->getAction()['subTitle'].'</label><br>';
//                             echo '</label>';
//                         }else
//                         {
//                             echo '<label class="checkbox" style="display:block;background:#37474f;margin-top:0;color:#fff">';
//                             echo '<input type="checkbox" name="permissions[]" value="'.$value->getName().'"> ';
//                             echo '<i class="icon-checkbox"></i>';
//                             echo '<label style="padding-right:8px">'.$value->getAction()['subTitle'].'</label><br>';
//                             echo '</label>';
//                         }
//                     }

//                     #foreach for sub links
//                     $routes = Route::getRoutes();
//                     foreach ($routes as $value)
//                     {
//                         if($value->getName() !== null && !isset($value->getAction()['icon']) || isset($value->getAction()['hasFather']))

//                         {
//                             if($value->getName() == $child)
//                             {
//                                 if(in_array($value->getName(),$arr))
//                                 {
//                                     echo '<label class="checkbox" style="display:block">';
//                                     echo '<input type="checkbox" name="permissions[]" checked  value="'.$value->getName().'"> ';
//                                     echo '<i class="icon-checkbox"></i>';
//                                     echo '<label style="padding-right:8px">'.$value->getAction()['title'].'</label><br>';
//                                     echo '</label>';
//                                 }else
//                                 {
//                                     echo '<label class="checkbox" style="display:block">';
//                                     echo '<input type="checkbox" name="permissions[]"  value="'.$value->getName().'"> ';
//                                     echo '<i class="icon-checkbox"></i>';
//                                     echo '<label style="padding-right:8px">'.$value->getAction()['title'].'</label><br>';
//                                     echo '</label>';
//                                 }
//                             }
//                         }
//                     }

//                 }
//                 echo '</div>';
//             }
//             if(!isset($value->getAction()['child']) && isset($value->getAction()['icon']) && !isset($value->getAction()['front']) && isset($value->getAction()['title']) && !isset($value->getAction()['hasFather']))
//             {
//                 $arr = [];
//                 $permission = Permission::where('role_id',$id)->select('permissions')->get();
//                 foreach($permission as $key=>$per)
//                 {
//                     $arr[$key] = $per->permissions;
//                 }

//                 echo '<div class="col-sm-2" style="border: 1px solid #000;margin-right:10px;margin-bottom:5px;padding:0;background:#eee">';
//                 if(in_array($value->getName(),$arr))
//                 {
//                     echo '<label class="checkbox" style="display:block;background:#37474f;margin-top:0;margin-bottom:0;color:#fff">';
//                     echo '<input type="checkbox" name="permissions[]" checked  value="'.$value->getName().'"> ';
//                     echo '<i class="icon-checkbox"></i>';
//                     echo '<label style="padding-right:8px ;margin-top:6px">'.$value->getAction()['title'].'</label><br>';
//                     echo '</label>';
//                 }else
//                 {
//                     echo '<label class="checkbox" style="display:block;background:#37474f;margin-top:0;margin-bottom:0;color:#fff">';
//                     echo '<input type="checkbox" name="permissions[]"  value="'.$value->getName().'"> ';
//                     echo '<i class="icon-checkbox"></i>';
//                      echo '<label style="padding-right:8px ;margin-top:6px">'.$value->getAction()['title'].'</label><br>';
//                      echo '</label>';
//                 }
//                 echo '</div>';
//             }
//     }
//   }
// }

//Area Helpers

function getEntityCountByType($type = "government")
{
    $count = 0;
    if ($type == 'government') {
        $governmentIds = \App\Models\Entity::whereType('government')->where('area_id', auth()->user()->userProfile->entity_id)->pluck('id');
        $count = \App\Models\Entity::whereType('office')->whereIn('government_id', $governmentIds)->count();
    } elseif ($type == 'office') {
        $governmentIds = \App\Models\Entity::whereType('government')->where('area_id', auth()->user()->userProfile->entity_id)->pluck('id');
        $officeIds = \App\Models\Entity::whereType('office')->whereIn('government_id', $governmentIds)->pluck('id');
        $count = \App\Models\Entity::whereType('school')->whereIn('office_id', $officeIds)->count();
    } elseif ($type == 'area') {
        $count = \App\Models\Entity::whereType('government')->where('area_id', auth()->user()->userProfile->entity_id)->count();
    }
    return $count;
}

function getMdoelCountByNameAndType($model = '', $type = '', $days = 1, $date = '')
{
    $count = 0;

    if ($model == 'visits' && $type == '') {
        $governmentIds = Entity::whereType('government')->where('area_id', auth()->user()->userProfile->entity_id)->pluck('id')->toArray();
        $officeIds = Entity::whereType('office')->whereIn('government_id', $governmentIds)->pluck('id')->toArray();
        $schoolsIds = Entity::whereType('school')->whereIn('office_id', $officeIds)->pluck('id')->toArray();
        $allIds = array_merge($governmentIds, $officeIds, $schoolsIds);
        $count = ReceptionDesk::whereIn('entity_id', $allIds)->count();
    } elseif ($model == 'warehouse') {
        $governmentIds = Entity::whereType('government')->where('area_id', auth()->user()->userProfile->entity_id)->pluck('id')->toArray();
        $officeIds = Entity::whereType('office')->whereIn('government_id', $governmentIds)->pluck('id')->toArray();
        $schoolsIds = Entity::whereType('school')->whereIn('office_id', $officeIds)->pluck('id')->toArray();
        $allIds = array_merge($governmentIds, $officeIds, $schoolsIds);
        $count = Warehouse::whereIn('entity_id', $allIds)->count();
    } elseif ($model == 'userformat') {

        $count = User::whereHas('userProfile', function ($query) {
            $userIds = [];
            array_push($userIds, auth()->user()->userProfile->entity_id);
            $governmentIds = Entity::whereType('government')->where('area_id', auth()->user()->userProfile->entity_id)->pluck('id')->toArray();
            $officeIds = Entity::whereType('office')->whereIn('government_id', $governmentIds)->pluck('id')->toArray();
            $schoolsIds = Entity::whereType('school')->whereIn('office_id', $officeIds)->pluck('id')->toArray();
            $allIds = array_merge($governmentIds, $officeIds, $schoolsIds, $userIds);
            $query->whereIn('entity_id', $allIds);
        })->whereHas('formats')->count();

    } elseif ($model == 'employee' || $model == 'teacher' || $model == 'students' && $date != '') {
        if ($model == 'employee') {
            $count = User::whereHas('userProfile', function ($query) use ($date) {
                $userIds = [];
                array_push($userIds, auth()->user()->userProfile->entity_id);
                $governmentIds = Entity::whereType('government')->where('area_id', auth()->user()->userProfile->entity_id)->pluck('id')->toArray();
                $officeIds = Entity::whereType('office')->whereIn('government_id', $governmentIds)->pluck('id')->toArray();
                $schoolsIds = Entity::whereType('school')->whereIn('office_id', $officeIds)->pluck('id')->toArray();
                $allIds = array_merge($governmentIds, $officeIds, $schoolsIds, $userIds);
                $query->whereIn('entity_id', $allIds)->where('app_title', 'employee')->whereBetween('created_at', [Carbon::now()->format('Y-m-d h:i A'), Carbon::now()->subDays(100)->format('Y-m-d h:i A')]);
            })->whereHas('formats')->count();

        } elseif ($model == 'teacher') {
            $count = User::whereHas('userProfile', function ($query) use ($date) {
                $userIds = [];
                array_push($userIds, auth()->user()->userProfile->entity_id);
                $governmentIds = Entity::whereType('government')->where('area_id', auth()->user()->userProfile->entity_id)->pluck('id')->toArray();
                $officeIds = Entity::whereType('office')->whereIn('government_id', $governmentIds)->pluck('id')->toArray();
                $schoolsIds = Entity::whereType('school')->whereIn('office_id', $officeIds)->pluck('id')->toArray();
                $allIds = array_merge($governmentIds, $officeIds, $schoolsIds, $userIds);
                $query->whereIn('entity_id', $allIds)->where('app_title', 'teacher')->whereBetween('created_at', [Carbon::now()->format('Y-m-d h:i A'), Carbon::now()->subDays(100)->format('Y-m-d h:i A')]);
            })->whereHas('formats')->count();
        } elseif ($model == 'students') {
            $count = User::whereHas('userProfile', function ($query) use ($date) {
                $userIds = [];
                array_push($userIds, auth()->user()->userProfile->entity_id);
                $governmentIds = Entity::whereType('government')->where('area_id', auth()->user()->userProfile->entity_id)->pluck('id')->toArray();
                $officeIds = Entity::whereType('office')->whereIn('government_id', $governmentIds)->pluck('id')->toArray();
                $schoolsIds = Entity::whereType('school')->whereIn('office_id', $officeIds)->pluck('id')->toArray();
                $allIds = array_merge($governmentIds, $officeIds, $schoolsIds, $userIds);
                $query->whereIn('entity_id', $allIds)->where('app_title', 'student')->whereBetween('created_at', [Carbon::now()->format('Y-m-d h:i A'), Carbon::now()->subDays(100)->format('Y-m-d h:i A')]);
            })->whereHas('formats')->count();
        }
    } elseif ($model == 'goodStudnets') {
        $count = User::whereHas('userProfile', function ($query) use ($date) {
            $userIds = [];
            array_push($userIds, auth()->user()->userProfile->entity_id);
            $governmentIds = Entity::whereType('government')->where('area_id', auth()->user()->userProfile->entity_id)->pluck('id')->toArray();
            $officeIds = Entity::whereType('office')->whereIn('government_id', $governmentIds)->pluck('id')->toArray();
            $schoolsIds = Entity::whereType('school')->whereIn('office_id', $officeIds)->pluck('id')->toArray();
            $allIds = array_merge($governmentIds, $officeIds, $schoolsIds, $userIds);
            $query->whereIn('entity_id', $allIds)->where('app_title', 'student')->whereBetween('created_at', [Carbon::now()->format('Y-m-d h:i A'), Carbon::now()->subDays(100)->format('Y-m-d h:i A')]);
        })->doesnthave('diseases')->count();
    } elseif ($model == 'studentWithDiesas') {
        $count = User::whereHas('userProfile', function ($query) use ($date) {
            $userIds = [];
            array_push($userIds, auth()->user()->userProfile->entity_id);
            $governmentIds = Entity::whereType('government')->where('area_id', auth()->user()->userProfile->entity_id)->pluck('id')->toArray();
            $officeIds = Entity::whereType('office')->whereIn('government_id', $governmentIds)->pluck('id')->toArray();
            $schoolsIds = Entity::whereType('school')->whereIn('office_id', $officeIds)->pluck('id')->toArray();
            $allIds = array_merge($governmentIds, $officeIds, $schoolsIds, $userIds);
            $query->whereIn('entity_id', $allIds)->where('app_title', 'student')->whereBetween('created_at', [Carbon::now()->format('Y-m-d h:i A'), Carbon::now()->subDays(100)->format('Y-m-d h:i A')]);
        })->whereHas('diseases')->count();
    }
    return $count;
}

function getModelCountWithDate($model, $isConditiondalDate = false, $dates = ['status' => null, 'addedTime' => null, 'numberOfAdded' => 0])
{
    if ($isConditiondalDate && $dates['status'] != null && $dates['addedTime'] != null) {
        if (substr($dates['addedTime'], 0, 3) == 'sub') {
            return $model->whereBetween($dates['status'], [Carbon::today()->{$dates['addedTime']}($dates['numberOfAdded']), Carbon::today()->{$dates['addedTime']}(1)])->count();
        } else {
            return $model->whereBetween($dates['status'], [Carbon::today(), Carbon::today()->{$dates['addedTime']}($dates['numberOfAdded'])])->count();
        }
    } else if ($isConditiondalDate) {
        return $model->whereDate('created_at', Carbon::today())->count();
    } else {
        return ($model == 'App\Models\Admin') ? $model->where('is_super_admin', 0)->count() : $model->count();
    }
}


function getModelDataByDate($model, $isConditiondalDate = false, $dates = ['status' => null, 'addedTime' => null, 'numberOfAdded' => 0])
{
    if ($isConditiondalDate && $dates['status'] != null && $dates['addedTime'] != null) {
        if (substr($dates['addedTime'], 0, 3) == 'sub') {
            return $model->whereBetween($dates['status'], [Carbon::today()->{$dates['addedTime']}($dates['numberOfAdded']), Carbon::today()->{$dates['addedTime']}(1)])->get();
        } else {
            return $model->whereBetween($dates['status'], [Carbon::today(), Carbon::today()->{$dates['addedTime']}($dates['numberOfAdded'])])->get();
        }
    } else if ($isConditiondalDate) {
        return $model->whereDate('created_at', Carbon::today())->get();
    } else {
        return ($model == 'App\Models\Admin') ? $model->where('is_super_admin', 0)->count() : $model->get();
    }
}

function getModelDataForManyDays($model, $dates = ['status' => null, 'addedTime' => null, 'numberOfAdded' => 0])
{
    $data = [];
    if ($model != null && $dates['addedTime'] != null) {
        if (substr($dates['addedTime'], 0, 3) == 'sub') {
            for ($x = $dates['numberOfAdded']; $x > 1; $x--) {
                $sub = substr(Carbon::today()->{$dates['addedTime']}($x)->format('Y-m-D'), 8, 3);
                $data[$sub] = $model->whereDate($dates['status'], Carbon::today()->{$dates['addedTime']}($x)->format('Y-m-d'))->count();
            }
        } else {
            for ($x = 0; $x < $dates['numberOfAdded']; $x++) {
                $subv = substr(Carbon::today()->{$dates['addedTime']}($x)->format('Y-m-D'), 8, 3);
                $data[$subv] = $model->whereDate($dates['status'], Carbon::today()->{$dates['addedTime']}($x)->format('Y-m-d'))->count();
            }
        }
        return $data;
    }
}

function getModelDataForMonth($model, $dates = ['status' => null, 'addedTime' => null])
{
    $data = [];
    $counter = 0;

    if ($model != null) {
        for ($x = 30; $x >= 1; $x--) {
            foreach (getCreatedAtArray($model) as $key => $value) {
                if (Carbon::today()->subDay($x)->format('Y-m-d') == $value) {
                    $counter++;
                } else {
                    $data[Carbon::today()->subDay($x)->format('Y-m-d')] = 0;
                }
            }
            $data[Carbon::today()->subDay($x)->format('Y-m-d')] = $counter;
            $counter = 0;
        }
        return $data;
    }
}

function getModelDataForWeek($model, $dates = ['status' => null, 'addedTime' => null])
{
    $data = [];
    $counter = 0;

    if ($model != null) {
        for ($x = 7; $x >= 1; $x--) {
            foreach (getCreatedAtArray($model) as $key => $value) {
                if (Carbon::today()->subDay($x)->format('Y-m-d') == $value) {
                    $counter++;
                } else {
                    $data[Carbon::today()->subDay($x)->format('Y-m-d')] = 0;
                }
            }
            $data[Carbon::today()->subDay($x)->format('Y-m-d')] = $counter;
            $counter = 0;
        }
        return $data;
    }
}

function getCreatedAtArray($model)
{
    $allCreated = $model->select('created_at')->get();
    foreach ($allCreated as $key => $date) {
        $dates[$key] = carbon($date->created_at, 'Y-m-d');
    }
    return $dates;
}

function CheckAttendance($user_id = 0, $date = '')
{
    if ($attendance = Attendance::where(['user_id' => $user_id, 'date' => $date])->first()) {
        return $attendance;
    }
    return '';
}

function getVehcileStudent($bus_id, $chair_number)
{
    $vehicle = VehicleChair::where('vehicle_id', $bus_id)->where('chair_number', $chair_number)->first();
    if ($vehicle) {
        $user = User::find($vehicle->user_id);
        return $user;
    } else {
        return false;
    }
}

function checkStudentBus($student_id, $bus_id)
{
    $countChair = VehicleChair::where('user_id', $student_id)->where('vehicle_id', $bus_id)->count();
    if ($countChair > 0) {
        return true;
    } else {
        return false;
    }
}


function checkStudentChair($student_id, $bus_id, $chair_number)
{
    $countChair = VehicleChair::where('user_id', $student_id)->where('vehicle_id', $bus_id)->where('chair_number', $chair_number)->count();
    if ($countChair > 0) {
        return true;
    } else {
        return false;
    }
}

function checkAttendStatus($lesson_attend, $student_id)
{

    $lesson_attend_result = LessonAttendResult::where(['lesson_attend_id' => $lesson_attend, 'user_id' => $student_id])->first();
    if ($lesson_attend_result) {
        return $lesson_attend_result->status;
    }
    return false;
}

function academic_result($precentage)
{
    if ($precentage >= 90) {
        return 'excellent';
    } elseif ($precentage < 90 && $precentage >= 80) {
        return 'very_good';
    } elseif ($precentage < 80 && $precentage >= 70) {
        return 'good';
    } elseif ($precentage < 70 && $precentage >= 50) {
        return 'acceptable';
    } else {
        return 'failure';
    }
}


function checkIsVote($studentTalentId)
{
    $countVotes = StudentVote::where('voter_id', auth()->user()->id)->where('student_talent_id', $studentTalentId)->count();
    if ($countVotes > 0) {
        return true;
    }

    return false;
}

